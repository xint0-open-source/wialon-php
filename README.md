# xint0/wialon-php

Wialon API client.

## Installation

### Requirements

- PHP 7.4 or later.
- PSR-18 HTTP client implementation.

### Install using Composer

Install with Guzzle 7 adapter:

```bash
composer require xint0/wialon-php php-http/guzzle7-adapter
```

If you already have a PSR-18 client in your project, you can install only the Wialon API client:

```bash
composer require xint0/wialon-php
```

## How to use

### Token authentication

First follow the instructions in the Wialon SDK documentation to obtain an authentication token.

### Login to Wialon service

```php
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

// You can specify a custom host.
$baseUri = 'https://hst-api.wialon.com';

$wialon = new Wialon($baseUri)

try {
    $token = 'your authentication token';
    $wialon->login($token);
    // After logging in to service a session ID is returned.
    $wialon->sessionId()
} catch (WialonException $exception) {
    // WialonException is thrown when request fails or service returns error response.
}
```

### Logout

```php
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

$token = 'token';

$wialon = new Wialon();

$wialon->login($token);
$wialon->logout();

$wialon->sessionId(); // Returns `null` after logging out.
```

### Get unit by name

```php
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

$token = 'token';

$wialon = new Wialon();

$wialon->login($token);

$data = $wialon->unitByName('Unit name'); // Returns associative array with response data.
```

### Get location by coordinates

```php
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

$token = 'token';

$wialon = new Wialon();

$wialon->login($token);

$longitude = -111.004287;
$latitude = 30.6141861;
$format = '32100';
$data = $wialon->getLocations($longitude, $latitude, $format); // Returns associative array with response data.

/*
$data == ["Magdalena, Sonora, Mexico"]
*/
```

## License

The Wialon API client is open source software licensed under the
[MIT license](https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE).
