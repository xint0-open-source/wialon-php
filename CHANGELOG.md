# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.2.1...master)

## [0.2.2](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.2.1...0.2.2) - 2024-07-19

### Changed

- `geofencesByResourceId` method in `Wialon` class accepts `ids` and `flags` to get specific geofence information.

## [0.2.1](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.2.0...0.2.1) - 2024-06-27

### Changed

- Support `psr/http-message` ^1.0 || ^2.0.
- Support `symfony/options-resolver` ^6.1 || ^7.0.

## [0.2.0](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.1.0...0.2.0) - 2024-01-05

### Added

- Add `Xint0\WialonPhp\Contracts\ResponseData` interface #1 #2 #6 #8 #11 #12
- Add `Xint0\WialonPhp\Wialon::gisGetRoute` method #2 #13
- Add `Xint0\WialonPhp\Responses\WialonResponse` class #2 #12
- Add `Xint0\WialonPhp\Responses\GisRoute` class #2 #11
- Add `Xint0\WialonPhp\Factories\RequestFactory::makeGisGetRouteRequest` method #2 #10
- Add `Xint0\WialonPhp\Requests\GisGetRouteParameters` class #2 #9
- Add `Xint0\WialonPhp\Wialon::gisIntelligentSearch` method #1 #8
- Add `Xint0\WialonPhp\Responses\GisIntelligentSearchItems` class #1 #6
- Add `Xint0\WialonPhp\Responses\GisIntelligentSearchItem` class #1 #5
- Add `Xint0\WialonPhp\Factories\RequestFactory::makeGisIntelligentSearchRequest` method #1 #4
- Add `Xint0\WialonPhp\Requests\GisIntelligentSearchParameters` class. #1 #3
- Add `Xint0\WialonPhp\Requests\AddressFormat` class.
- Add `Xint0\WialonPhp\Requests\GisGeocodeRequestParameters` class.

### Changed

- Upgrade PHPCS and PHPCBF to 3.8.0.
- Replace PHPUnit annotations with attributes.
- Drop support for PHP 8.0.
- Include PHP 8.3 in CI pipeline.
- Upgrade `psr/http-message` to version 2.0.
- Upgrade PHPUnit to version 10.5.

## [0.1.0](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.9...0.1.0) - 2023-08-03

### Changed

- Drop support for PHP 7.4.
- Replace deprecated `php-http/message-factory` with `psr/http-factory-implementation`.
- Use PHPStan docker image instead of PHAR, include `phpstan/phpstan-phpunit` extension.

## [0.0.9](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.8...0.0.9) - 2023-05-16

### Added

- `getUnits` method in `Wialon` class to return all units.

## [0.0.8](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.7...0.0.8) - 2023-04-05

### Changed

- `RequestFactory` construct accepts `UriInterface` instance.
- `Wialon@baseUri` can update service configured base URI.

## [0.0.7](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.6...0.0.7) - 2023-03-29

### Added

- `geofencesByResourceId` method in `Wialon` class to return geofences in specified resource.

## [0.0.6](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.5...0.0.6) - 2023-02-24

### Added

- `baseUri` method to `Wialon` class to return configured base URI value.

## [0.0.5](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.4...0.0.5) - 2023-01-09

### Added

- PHPDoc comments for public methods.

## [0.0.4](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.3...0.0.4) - 2023-01-08

### Added

- Add license to source files.
- Add README
- Add CHANGELOG

## [0.0.3](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.2...0.0.3) - 2022-12-08

### Added

- Implement getLocations.

## [0.0.2](https://gitlab.com/xint0-open-source/wialon-php/-/compare/0.0.1...0.0.2) - 2022-12-08

### Added

- Implement unitByName.

## [0.0.1](https://gitlab.com/xint0-open-source/wialon-php/-/compare/7b2667d8f2f085f2fc88b619708f447b6da76c33...0.0.1) - 2022-12-07

### Added

- Initial release.
  - Implement login.
  - Implement logout.