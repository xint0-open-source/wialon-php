<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2024. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Contracts;

/**
 * Wialon service response data.
 */
interface ResponseData
{
    /**
     * Creates instance of the implementing class from the JSON array decoded from the response body.
     *
     * @param  mixed  $source  The decoded JSON array.
     *
     * @return ResponseData
     */
    public static function fromResponseJson(mixed $source): ResponseData;
}