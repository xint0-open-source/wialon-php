<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Factories;

use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientInterface;

class HttpClientFactory
{
    public function make(): ClientInterface
    {
        return Psr18ClientDiscovery::find();
    }
}