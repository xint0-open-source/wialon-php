<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Factories;

use Http\Discovery\Psr17FactoryDiscovery;
use InvalidArgumentException;
use JsonException;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriInterface;
use Throwable;
use Xint0\WialonPhp\Requests\GisGeocodeRequestParameters;
use Xint0\WialonPhp\Requests\GisGetRouteParameters;
use Xint0\WialonPhp\Requests\GisIntelligentSearchParameters;

class RequestFactory
{
    private const DEFAULT_BASE_URI = 'https://hst-api.wialon.com';
    private const PATH_AJAX = 'wialon/ajax.html';
    private const PATH_GIS_GEOCODE = 'gis_geocode';
    protected UriInterface $baseUri;
    protected RequestFactoryInterface $requestFactory;
    protected StreamFactoryInterface $streamFactory;

    /**
     * Creates a new request factory
     *
     * @param  string|UriInterface|null  $baseUri  Service base URI
     *
     * @throws InvalidArgumentException When custom service base URI is not valid.
     */
    public function __construct(UriInterface|string $baseUri = null)
    {
        $this->initializeBaseUri($baseUri);
        $this->requestFactory = Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = Psr17FactoryDiscovery::findStreamFactory();
    }

    /**
     * Gets or sets service base URI.
     *
     * When optional new value parameter is specified the service base URI
     * value is updated if valid.
     *
     * If the new value is not valid or `null` (not specified) the service
     * base URI is not updated.
     *
     * @param  string|UriInterface|null  $new_value  Optional new service base
     * URI value.
     *
     * @return UriInterface The current service base URI.
     */
    public function baseUri(UriInterface|string $new_value = null): UriInterface
    {
        if (null !== $new_value) {
            $this->setBaseUri($new_value);
        }
        return $this->baseUri;
    }

    /**
     * @throws JsonException
     */
    public function makeGeofencesByResourceIdRequest(
        int $resource_id,
        string $session_id,
        array $ids = [],
        int $flags = 0x10
    ): RequestInterface {
        return $this->makeRequest(
            self::encodeParams($this->getResourceGetZoneDataParams($resource_id, $session_id, $ids, $flags))
        );
    }

    /**
     * @param  GisGeocodeRequestParameters  $params
     *
     * @return RequestInterface
     * @throws JsonException
     */
    public function makeGisGeocodeRequest(GisGeocodeRequestParameters $params): RequestInterface
    {
        return $this->makeRequest($params->urlEncode(), self::PATH_GIS_GEOCODE);
    }

    public function makeGisGetRouteRequest(GisGetRouteParameters $params): RequestInterface
    {
        return $this->makeRequest($params->urlEncode(), 'gis_get_route');
    }

    public function makeGisIntelligentSearchRequest(GisIntelligentSearchParameters $params): RequestInterface
    {
        return $this->makeRequest($params->urlEncode(), 'gis_searchintelli');
    }

    /**
     * @throws JsonException
     */
    public function makeLoginRequest(string $token): RequestInterface
    {
        return $this->makeRequest(self::encodeParams($this->getLoginRequestParams($token)));
    }

    /**
     * @throws JsonException
     */
    public function makeLogoutRequest(string $session_id): RequestInterface
    {
        return $this->makeRequest(self::encodeParams($this->getLogoutRequestParams($session_id)));
    }

    /**
     * @param  array<string,mixed>  $params
     * @param  string  $session_id
     *
     * @return RequestInterface
     * @throws JsonException
     */
    public function makeSearchItemsRequest(array $params, string $session_id): RequestInterface
    {
        return $this->makeRequest(self::encodeParams($this->getSearchItemsRequestParams($params, $session_id)));
    }

    /**
     * @param  array<string,bool|float|int|string|null|\JsonSerializable|array>  $params
     *
     * @return string
     * @throws JsonException
     */
    private static function encodeParams(array $params): string
    {
        return join(
            '&',
            array_map(
                fn($k, $v) => $k . '=' . urlencode(is_object($v) || is_array($v) ? json_encode($v, JSON_THROW_ON_ERROR) : strval($v)),
                array_keys($params),
                array_values($params)
            )
        );
    }

    private function initializeBaseUri(UriInterface|string|null $uri): void
    {
        if ($uri instanceof UriInterface) {
            $baseUri = $uri;
        } else {
            $uri = $uri ?? self::DEFAULT_BASE_URI;
            try {
                $baseUri = Psr17FactoryDiscovery::findUriFactory()->createUri($uri);
            } catch (Throwable $exception) {
                throw new InvalidArgumentException(
                    sprintf('The base URI value "%s" is not valid.', $uri),
                    0,
                    $exception
                );
            }
        }
        if (! $this->validateBaseUri($baseUri)) {
            throw new InvalidArgumentException(sprintf('The base URI value "%s" is not valid.', $uri), 0);
        }
        $this->baseUri = $baseUri;
    }

    private function makeRequest(string $content, string $path = self::PATH_AJAX): RequestInterface
    {
        $body = $this->streamFactory->createStream($content);
        return $this->requestFactory
            ->createRequest('POST', $this->baseUri->withPath($path))
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', 'application/x-www-form-urlencoded')
            ->withBody($body);
    }

    private function setBaseUri(UriInterface|string $newBaseUri): void
    {
        if ($newBaseUri instanceof UriInterface) {
            $baseUri = $newBaseUri;
        } else {
            $uriFactory = Psr17FactoryDiscovery::findUriFactory();
            try {
                $baseUri = $uriFactory->createUri($newBaseUri);
            } catch (Throwable) {
                return;
            }
        }
        if ($this->validateBaseUri($baseUri)) {
            $this->baseUri = $baseUri;
        }
    }

    private function validateBaseUri(UriInterface $uri): bool
    {
        return ('' !== $uri->getScheme()) && ('' !== $uri->getHost());
    }

    /**
     * @param  string  $token
     *
     * @return array{svc:string,params:string}
     * @throws JsonException
     */
    private function getLoginRequestParams(string $token): array
    {
        return [
            'svc' => 'token/login',
            'params' => json_encode(['token' => urlencode($token)], JSON_THROW_ON_ERROR),
        ];
    }

    /**
     * @param  string  $session_id
     *
     * @return array{svc:string,params:string,sid:string}
     */
    private function getLogoutRequestParams(string $session_id): array
    {
        return [
            'svc' => 'core/logout',
            'params' => '{}',
            'sid' => $session_id,
        ];
    }

    /**
     * @param  int  $resource_id
     * @param  string  $session_id
     * @param  array  $ids
     * @param  int  $flags
     *
     * @return array{svc:string,params:string,sid:string}
     * @throws JsonException
     */
    private function getResourceGetZoneDataParams(int $resource_id, string $session_id, array $ids = [], int $flags = 0x10): array
    {
        $params = [
            'itemId' => $resource_id,
            'flags' => $flags,
        ];
        if (count($ids) > 0) {
            $params['col'] = $ids;
        }
        return [
            'svc' => 'resource/get_zone_data',
            'params' => json_encode($params, JSON_THROW_ON_ERROR),
            'sid' => $session_id,
        ];
    }

    /**
     * @param  array<string,mixed>  $params
     * @param  string  $session_id
     *
     * @return array{svc:string,params:string,sid:string}
     * @throws JsonException
     */
    private function getSearchItemsRequestParams(array $params, string $session_id): array
    {
        return [
            'svc' => 'core/search_items',
            'params' => json_encode($params, JSON_THROW_ON_ERROR),
            'sid' => $session_id,
        ];
    }
}
