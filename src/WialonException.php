<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * Exception thrown if a Wialon client error occurs.
 */
class WialonException extends \RuntimeException
{
    private const SERVICE_ERROR_MESSAGES = [
        0 => 'Success.',
        1 => 'Invalid session.',
        2 => 'Invalid service name.',
        3 => 'Invalid result.',
        4 => 'Invalid input.',
        5 => 'Error performing request.',
        6 => 'Unknown error.',
        7 => 'Access denied.',
        8 => 'Invalid user name or password.',
        9 => 'Authorization server is unavailable.',
        10 => 'Reached limit of concurrent requests.',
        11 => 'Password reset error.',
        14 => 'Billing error.',
        1001 => 'No messages for selected interval.',
        1002 => 'Item with same properties already exists or cannot be created according to billing restrictions.',
        1003 => 'Only one request is allowed at the moment.',
        1004 => 'Limit of messages has been exceeded.',
        1005 => 'Execution time has exceeded the limit.',
        1006 => 'Exceeding the limit of attempts to enter a two-factor authorization code.',
        1011 => 'Your IP has changed or session has expired.',
        2006 => 'Not possible to transfer unit to this account.',
        2008 => 'User does not have access to unit (due to transferring to new account).',
        2014 => 'Selected user is a creator for some system objects, thus cannot be bound to new account.',
        2015 => 'Cannot delete sensor because it is used in another sensor or advanced properties of unit.',
    ];

    /**
     * Create exception to be thrown when HTTP client throws exception.
     *
     * @param  ClientExceptionInterface  $exception
     *
     * @return self
     */
    public static function fromClientExceptionInterface(ClientExceptionInterface $exception): self
    {
        return new self('Service request failed.', 9000, $exception);
    }

    /**
     * Create exception to be thrown when client does not have a session ID.
     *
     * @return self
     */
    public static function noSessionException(): self
    {
        return new self('Client does not have session ID.', 9001);
    }

    /**
     * Create exception to be thrown when response includes error code.
     *
     * @param  int  $error_code
     *
     * @return self
     */
    public static function serviceError(int $error_code): self
    {
        $error_message = array_key_exists($error_code, self::SERVICE_ERROR_MESSAGES) ?
            self::SERVICE_ERROR_MESSAGES[$error_code] : "Unknown service error code $error_code.";
        return new self("Service error: $error_message", $error_code);
    }

    /**
     * Create exception to be thrown when response HTTP status code is client error code.
     *
     * @param  int  $http_status
     *
     * @return self
     */
    public static function clientErrorHttpCode(int $http_status): self
    {
        $error_message = "Client error code: $http_status.";
        return new self($error_message, 10000 + $http_status);
    }

    /**
     * Create exception to be thrown when response HTTP status code is server error code.
     *
     * @param  int  $http_status
     *
     * @return self
     */
    public static function serverErrorHttpCode(int $http_status): self
    {
        $error_message = "Server error code: $http_status.";
        return new self($error_message, 20000 + $http_status);
    }

    /**
     * Create exception to be thrown when response HTTP status code is not expected code.
     *
     * @param  int  $http_status
     *
     * @return self
     */
    public static function unexpectedHttpCode(int $http_status): self
    {
        $error_message = "Unexpected code: $http_status.";
        return new self($error_message, 30000 + $http_status);
    }
}