<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp;

use InvalidArgumentException;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Xint0\WialonPhp\Factories\HttpClientFactory;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Requests\AddressFormat;
use Xint0\WialonPhp\Requests\GisGeocodeRequestParameters;
use Xint0\WialonPhp\Requests\GisGetRouteParameters;
use Xint0\WialonPhp\Requests\GisIntelligentSearchParameters;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItems;
use Xint0\WialonPhp\Responses\GisRoute;
use Xint0\WialonPhp\Responses\WialonResponse;

class Wialon
{
    private ClientInterface $httpClient;
    private RequestFactory $requestFactory;
    private ?string $sessionId = null;
    /** @var array<string,mixed>|null User data */
    private ?array $user = null;

    /**
     * Creates a new instance of the Wialon service.
     *
     * @param  string|UriInterface|null  $baseUri  Optional custom service base
     * URI. If not specified the service base URI is
     * `https://hst-api.wialon.com`.
     * @param  ClientInterface|null  $client  Optional PSR-7 HTTP client
     * implementation. If not specified the service tries to discover an
     * implementation.
     *
     * @throws InvalidArgumentException When `$baseUri` is not valid.
     */
    public function __construct(UriInterface|string $baseUri = null, ?ClientInterface $client = null)
    {
        $this->initializeHttpClient($client);
        $this->initializeRequestFactory($baseUri);
    }

    /**
     * Gets or sets the service base URI.
     *
     * When the optional `$new_uri` value parameter is specified the service
     * base URI is updated if valid.
     *
     * If the `$new_uri` value is not valid or is `null` (not specified) the
     * service base URI is not updated.
     *
     * @param  string|UriInterface|null  $new_uri  The new service base URI value.
     *
     * @return UriInterface
     */
    public function baseUri(UriInterface|string $new_uri = null): UriInterface
    {
        return $this->requestFactory->baseUri($new_uri);
    }

    /**
     * Returns list of geofences available in the specified resource.
     *
     * @param  int  $resource_id
     * @param  array  $ids
     * @param  int  $flags
     *
     * @return array<int,array<string,mixed>>
     *
     * @throws WialonException When there is no active session.
     * @throws WialonException When request fails.
     * @throws WialonException When response status is not success.
     * @throws WialonException When response contains service error code.
     * @throws JsonException
     */
    public function geofencesByResourceId(int $resource_id, array $ids = [], int $flags = 0x10): array
    {
        $this->validateSession();
        return $this->parseGeofencesByResourceIdResponse(
            $this->sendRequest(
                $this->requestFactory->makeGeofencesByResourceIdRequest($resource_id, $this->sessionId, $ids, $flags)
            )
        );
    }

    /**
     * Returns list of location addresses based on the specified position.
     *
     * @param  float  $longitude
     * @param  float  $latitude
     * @param  string  $format
     *
     * @return array<int,string>
     *
     * @throws WialonException When there is no active session; or when request fails;
     * or when response status is not success; or when response contains error code.
     * @throws JsonException
     */
    public function getLocations(float $longitude, float $latitude, string $format = '54321'): array
    {
        $this->validateSession();
        $params = new GisGeocodeRequestParameters($longitude, $latitude, new AddressFormat($format), $this->user['id']);
        $response = $this->sendRequest($this->requestFactory->makeGisGeocodeRequest($params));
        return $this->parseGetLocationsResponse($response);
    }

    /**
     * Returns list of units.
     *
     * @return array<int,array<string,mixed>>
     *
     * @throws WialonException When there is no active session; or when request fails;
     * or when response status is not success; or when response contains error code.
     * @throws JsonException
     */
    public function getUnits(): array
    {
        $this->validateSession();
        $params = [
            'spec' => [
                'itemsType' => 'avl_unit',
                'propName' => 'sys_name',
                'propValueMask' => '*',
                'sortType' => 'sys_name',
                'propType' => '',
            ],
            'force' => 1,
            'flags' => 0x101,
            'from' => 0,
            'to' => 0,
        ];
        $response = $this->sendRequest($this->requestFactory->makeSearchItemsRequest($params, $this->sessionId));
        return $this->validateAndDecodeResponse($response);
    }

    /**
     * Returns route between two points.
     *
     * @return WialonResponse<GisRoute>
     */
    public function gisGetRoute(
        float $latitude1,
        float $longitude1,
        float $latitude2,
        float $longitude2
    ): WialonResponse {
        $this->validateSession();
        $requestParameters = new GisGetRouteParameters(
            $latitude1,
            $longitude1,
            $latitude2,
            $longitude2,
            1,
            $this->user['id'],
        );
        $response = $this->sendRequest($this->requestFactory->makeGisGetRouteRequest($requestParameters));
        return new WialonResponse(GisRoute::class, $response);
    }

    /**
     * Searches coordinates for the specified address phrase.
     *
     * @return WialonResponse<GisIntelligentSearchItems>
     * @throws WialonException When there is no active session;
     */
    public function gisIntelligentSearch(
        string $phrase,
        int $count = 10,
        int $indexFrom = 0,
    ): WialonResponse {
        $this->validateSession();
        $params = new GisIntelligentSearchParameters($phrase, $this->user['id'], $count, $indexFrom);
        $response = $this->sendRequest($this->requestFactory->makeGisIntelligentSearchRequest($params));

        return new WialonResponse(GisIntelligentSearchItems::class, $response);
    }

    /**
     * Returns the HTTP client used to send requests to the Wialon web service.
     *
     * @return ClientInterface
     */
    public function httpClient(): ClientInterface
    {
        return $this->httpClient;
    }

    /**
     * Login to Wialon service.
     *
     * Returns `true` on success; `false` when service request fails.
     *
     * @param  string  $token
     *
     * @return bool
     *
     * @throws WialonException When request fails; or when response status is not success;
     * or when response contains error code.
     * @throws JsonException
     */
    public function login(string $token): bool
    {
        $response = $this->sendRequest($this->requestFactory->makeLoginRequest($token));
        $this->parseLoginResponse($response);
        return $this->sessionId !== null;
    }

    /**
     * Ends the current active session.
     *
     * @return void
     *
     * @throws WialonException When there is no active session; or when request fails;
     * or when response status is not success; or when response contains error code.
     * @throws JsonException
     */
    public function logout(): void
    {
        $this->validateSession();
        $response = $this->sendRequest($this->requestFactory->makeLogoutRequest($this->sessionId));
        $this->parseLogoutResponse($response);
    }

    /**
     * Returns the current session identifier; `null` when there is no current session.
     *
     * @return string|null
     */
    public function sessionId(): ?string
    {
        return $this->sessionId;
    }

    /**
     * Returns list of units that match the specified name pattern.
     *
     * @param  string  $name
     *
     * @return array<int,array<string,mixed>>
     *
     * @throws WialonException When request fails; or when response code is not success;
     * or when response contains error code.
     * @throws JsonException
     */
    public function unitByName(string $name): array
    {
        $this->validateSession();
        $params = [
            'spec' => [
                'itemsType' => 'avl_unit',
                'propName' => 'sys_name',
                'propValueMask' => $name,
                'sortType' => '',
                'propType' => '',
                'or_logic' => false,
            ],
            'force' => 1,
            'flags' => 0x411,
            'from' => 0,
            'to' => 0,
        ];
        $response = $this->sendRequest($this->requestFactory->makeSearchItemsRequest($params, $this->sessionId));
        return $this->parseUnitByNameResponse($response);
    }

    /**
     * @param  RequestInterface  $request
     *
     * @return ResponseInterface
     *
     * @throws WialonException When HTTP request fails.
     */
    private function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            return $this->httpClient->sendRequest($request);
        } catch (ClientExceptionInterface $exception) {
            throw WialonException::fromClientExceptionInterface($exception);
        }
    }

    private function initializeHttpClient(?ClientInterface $client = null): void
    {
        if ($client instanceof ClientInterface) {
            $this->httpClient = $client;
            return;
        }

        $factory = new HttpClientFactory();
        $this->httpClient = $factory->make();
    }

    /**
     * @param  string|UriInterface|null  $baseUri
     *
     * @return void
     *
     * @throws InvalidArgumentException When `$baseUri` is not valid.
     */
    private function initializeRequestFactory(UriInterface|string $baseUri = null): void
    {
        $this->requestFactory = new RequestFactory($baseUri);
    }

    /**
     * Verifies session ID is valid.
     *
     * @return void
     *
     * @throws WialonException When session ID is `null`.
     */
    private function validateSession(): void
    {
        if ($this->sessionId === null) {
            throw WialonException::noSessionException();
        }
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return array<int,array<string,mixed>>
     */
    private function parseGeofencesByResourceIdResponse(ResponseInterface $response): array
    {
        return $this->validateAndDecodeResponse($response);
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return array<int,string|array<string,mixed>>
     *
     * @throws WialonException When response HTTP status code is not success;
     * or when response contains error code.
     */
    private function parseGetLocationsResponse(ResponseInterface $response): array
    {
        return $this->validateAndDecodeResponse($response);
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return void
     *
     * @throws WialonException When response HTTP status code is not success;
     * or when response contains error code.
     */
    private function parseLoginResponse(ResponseInterface $response): void
    {
        $data = $this->validateAndDecodeResponse($response);
        $this->sessionId = $data['eid'];
        $this->user = $data['user'];
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return void
     *
     * @throws WialonException When response HTTP status code is not success;
     * or when response contains error code.
     */
    private function parseLogoutResponse(ResponseInterface $response): void
    {
        $data = $this->validateAndDecodeResponse($response);
        if (array_key_exists('error', $data) && $data['error'] === 0) {
            $this->sessionId = null;
            $this->user = null;
        }
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return array<int,array<string,mixed>>
     *
     * @throws WialonException When response HTTP status code is not success;
     * or when response contains error code.
     */
    private function parseUnitByNameResponse(ResponseInterface $response): array
    {
        return $this->validateAndDecodeResponse($response);
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return array<int,string|array<string,mixed>>|array<string,mixed>
     *
     * @throws WialonException When response HTTP status code is not success;
     * or when response body has error code.
     */
    private function validateAndDecodeResponse(ResponseInterface $response): array
    {
        $this->validateResponseStatusCode($response->getStatusCode());
        $data = json_decode((string)$response->getBody(), true);
        $this->validateResponseError($data);
        return $data;
    }

    /**
     * @param  int  $http_status_code
     *
     * @return void
     *
     * @throws WialonException When HTTP response status code is not success.
     */
    private function validateResponseStatusCode(int $http_status_code): void
    {
        if ($http_status_code >= 400 and $http_status_code < 500) {
            throw WialonException::clientErrorHttpCode($http_status_code);
        }

        if ($http_status_code >= 500 and $http_status_code < 600) {
            throw WialonException::serverErrorHttpCode($http_status_code);
        }

        if ($http_status_code !== 200) {
            throw WialonException::unexpectedHttpCode($http_status_code);
        }
    }

    /**
     * Validates if response data contains error code.
     *
     * @param  array<string,mixed>  $data
     *
     * @return void
     *
     * @throws WialonException When response data contains error code.
     */
    private function validateResponseError(array $data): void
    {
        $error_code = array_key_exists('error', $data) ? intval($data['error']) : 0;
        if ($error_code !== 0) {
            throw WialonException::serviceError($error_code);
        }
    }
}
