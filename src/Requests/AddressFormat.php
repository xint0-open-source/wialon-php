<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2023 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Requests;

use DomainException;
use LengthException;

class AddressFormat
{
    public readonly string $normalizedFormat;
    public readonly int $intValue;

    /**
     * @param  string  $format  The address format of up to 5 positions.
     * {@see https://sdk.wialon.com/wiki/en/local/remoteapi2204/apiref/requests/address}
     *
     * @throws DomainException When format contains digits other than 1-5.
     * @throws LengthException When format is empty or exceeds 5 characters.
     */
    public function __construct(public readonly string $format)
    {
        $this->validateFormat();
        $this->normalizedFormat = substr($this->format . '00000', 0, 5);
        $this->intValue = $this->asInt();
    }

    /**
     * @throws DomainException When format contains digits other that 1-5.
     * @throws LengthException When format exceeds 5 digits.
     */
    protected function validateFormat(): void
    {
        $length = strlen($this->format);
        if ($length === 0 || $length > 5) {
            throw new LengthException(
                sprintf('The specified format length (%d) is not between 1 and 5.', $length)
            );
        }
        if (preg_match('/^[0-5]{1,5}$/', $this->format) !== 1) {
            throw new DomainException(sprintf('The specified format "%s" is invalid.', $this->format));
        }
    }

    public function asInt(): int
    {
        $f = 0;
        for ($i = 0; $i < 5; $i++) {
            $f <<= 3;
            $f += intval(substr($this->normalizedFormat, $i, 1));
        }
        return $f << 16;
    }
}
