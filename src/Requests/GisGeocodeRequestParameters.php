<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2023 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Requests;

use JsonException;

class GisGeocodeRequestParameters
{
    public function __construct(
        public float $longitude,
        public float $latitude,
        public readonly AddressFormat $addressFormat,
        public int $user_id,
    ) {
    }

    /**
     * @throws JsonException
     */
    public function urlEncode(): string
    {
        return join('&', [
            'coords=' . urlencode(
                json_encode([['lon' => $this->longitude, 'lat' => $this->latitude]], JSON_THROW_ON_ERROR)
            ),
            'flags=' . urlencode(strval($this->addressFormat->intValue)),
            'uid=' . urlencode(strval($this->user_id)),
        ]);
    }
}
