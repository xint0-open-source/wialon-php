<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2023 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Requests;

class GisIntelligentSearchParameters
{
    public function __construct(
        public readonly string $phrase,
        public readonly int $userId,
        public readonly int $count = 10,
        public readonly int $indexFrom = 0,
    ) {
    }

    public function urlEncode(): string
    {
        return 'phrase=' . urlencode($this->phrase) . '&count=' . $this->count .
            ($this->indexFrom > 0 ? '&indexFrom=' . $this->indexFrom : '') .
            '&uid=' . $this->userId;
    }
}