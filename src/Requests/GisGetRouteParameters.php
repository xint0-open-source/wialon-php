<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Requests;

class GisGetRouteParameters
{
    public function __construct(
        public readonly float $latitudeOne,
        public readonly float $longitudeOne,
        public readonly float $latitudeTwo,
        public readonly float $longitudeTwo,
        public readonly int $flags,
        public readonly int $userId,
    ) {
    }

    public function urlEncode(): string
    {
        return 'lat1=' . urlencode(strval($this->latitudeOne)) .
            '&lon1=' . urlencode(strval($this->longitudeOne)) .
            '&lat2=' . urlencode(strval($this->latitudeTwo)) .
            '&lon2=' . urlencode(strval($this->longitudeTwo)) .
            '&flags=' . urlencode(strval($this->flags)) .
            '&uid=' . urlencode(strval($this->userId));
    }
}