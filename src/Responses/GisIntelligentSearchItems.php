<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Responses;

use ArrayAccess;
use BadMethodCallException;
use Countable;
use IteratorAggregate;
use SplFixedArray;
use Traversable;
use Xint0\WialonPhp\Contracts\ResponseData;

/**
 * Read-only list of GIS intelligent search items.
 *
 * @implements IteratorAggregate<int,GisIntelligentSearchItem>
 * @implements ArrayAccess<int,GisIntelligentSearchItem>
 */
class GisIntelligentSearchItems implements IteratorAggregate, ArrayAccess, Countable, ResponseData
{
    /** @var SplFixedArray<GisIntelligentSearchItem> $items */
    private SplFixedArray $items;

    /**
     * Create a new read-only list of GIS intelligent search items from the specified values.
     *
     * @param  GisIntelligentSearchItem  ...$items  Items to include in the read-only list.
     */
    public function __construct(GisIntelligentSearchItem ...$items)
    {
        $this->items = SplFixedArray::fromArray($items);
    }

    /**
     * @return Traversable<int,GisIntelligentSearchItem>
     */
    public function getIterator(): Traversable
    {
        return $this->items->getIterator();
    }

    public function count(): int
    {
        return $this->items->count();
    }

    /**
     * @param  int  $offset
     *
     * @return bool
     */
    public function offsetExists(mixed $offset): bool
    {
        return $this->items->offsetExists($offset);
    }

    /**
     * Retrieve item at offset.
     *
     * @param  int  $offset
     */
    public function offsetGet(mixed $offset): GisIntelligentSearchItem
    {
        return $this->items->offsetGet($offset);
    }

    /**
     * Not supported on read-only list.
     *
     * @throws BadMethodCallException
     * @return never-return
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        throw new BadMethodCallException('Cannot set value of read-only list.');
    }

    /**
     * Not supported on read-only list.
     *
     * @throws BadMethodCallException
     * @return never-return
     */
    public function offsetUnset(mixed $offset): void
    {
        throw new BadMethodCallException('Cannot unset value of read-only list.');
    }

    public static function fromResponseJson(mixed $source): self
    {
        $items = array_merge(...array_filter(array_map(
            fn ($item) => is_array($item) && array_key_exists('items', $item) ? $item['items'] : null,
            $source,
        )));
        $items = array_map(function ($item) {
            $item['latitude'] = $item['y'];
            unset($item['y']);
            $item['longitude'] = $item['x'];
            unset($item['x']);
            return $item;
        }, $items);
        return new self(...array_map(fn ($item) => new GisIntelligentSearchItem($item), $items));
    }
}
