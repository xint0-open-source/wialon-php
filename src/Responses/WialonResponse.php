<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

namespace Xint0\WialonPhp\Responses;

use JsonException;
use Psr\Http\Message\ResponseInterface;
use ReflectionClass;
use ReflectionException;
use Xint0\WialonPhp\Contracts\ResponseData;
use Xint0\WialonPhp\WialonException;

/**
 * @template-covariant  TData of ResponseData  The type of response data.
 */
class WialonResponse
{
    /** @var ResponseData The response data */
    protected ResponseData $data;

    /**
     * @param  class-string<TData>  $dataClass
     *
     * @throws WialonException When response HTTP status code is not success.
     * @throws ReflectionException When data class does not exist.
     */
    public function __construct(string $dataClass, public readonly ResponseInterface $response)
    {
        $this->verifyResponseHttpStatusCode();
        $this->data = $this->decodeResponse($dataClass);
    }

    /**
     * The response data.
     *
     * @return TData
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * @throws WialonException When HTTP status code is not success.
     */
    protected function verifyResponseHttpStatusCode(): void
    {
        $httpStatusCode = $this->response->getStatusCode();
        if ($httpStatusCode !== 200) {
            if ($httpStatusCode >= 400 && $httpStatusCode < 500) {
                throw WialonException::clientErrorHttpCode($httpStatusCode);
            }
            if ($httpStatusCode >= 500 && $httpStatusCode < 600) {
                throw WialonException::serverErrorHttpCode($httpStatusCode);
            }
            throw WialonException::unexpectedHttpCode($httpStatusCode);
        }
    }

    /**
     * @param  class-string<ResponseData>  $dataClass
     *
     * @throws WialonException When response body cannot be decoded as JSON.
     * @throws ReflectionException When class does not exist.
     */
    protected function decodeResponse(string $dataClass): ResponseData
    {
        try {
            $decoded = json_decode($this->response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new WialonException('Could not decode response.', 9002, $exception);
        }

        if (array_key_exists('error', $decoded)) {
            throw WialonException::serviceError((int)$decoded['error']);
        }

        $reflectionClass = new ReflectionClass($dataClass);
        if ($reflectionClass->implementsInterface(ResponseData::class)) {
            return $dataClass::fromResponseJson($decoded);
        }

        return new $dataClass($decoded);
    }
}
