<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Responses;

use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Xint0\WialonPhp\Contracts\ResponseData;

class GisRoute implements ResponseData
{
    /** @var string The request status. */
    public readonly string $status;
    /** @var string The route encoded as a Google Polyline. */
    public readonly string $points;
    /** @var float The route distance in meters. */
    public readonly float $distance;
    /** @var int The route duration in seconds. */
    public readonly int $duration;

    /** @var array<class-string, OptionsResolver> $resolvers */
    private static array $resolvers;

    /**
     * @param  array{
     *     distance: array{
     *         value: float
     *     },
     *     duration: array{
     *         value: int,
     *     },
     *     points: string,
     *     status: string,
     * }  $data
     *
     * @throws InvalidOptionsException When attribute is of wrong type.
     * @throws MissingOptionsException When required attribute is missing.
     */
    public function __construct(array $data)
    {
        $resolver = self::getResolver(get_class($this));
        /** @var array{
         *     distance: array{
         *         value: float,
         *     },
         *     duration: array{
         *         value: int,
         *     },
         *     points: string,
         *     status: string,
         * } $options
         */
        $options = $resolver->resolve($data);
        $this->distance = $options['distance']['value'];
        $this->duration = $options['duration']['value'];
        $this->points = $options['points'];
        $this->status = $options['status'];
    }

    private static function getResolver(string $class): OptionsResolver
    {
        if (! isset(self::$resolvers[$class])) {
            self::$resolvers[$class] = new OptionsResolver();
            static::configureResolver(self::$resolvers[$class]);
        }

        return self::$resolvers[$class];
    }

    public static function configureResolver(OptionsResolver $resolver): void
    {
        $resolver->define('status')
            ->allowedTypes('string')
            ->required()
            ->define('points')
            ->allowedTypes('string')
            ->required()
            ->define('distance')
            ->default(function (OptionsResolver $distanceResolver) {
                $distanceResolver->define('value')
                    ->allowedTypes('float')
                    ->required()
                    ->ignoreUndefined();
            })
            ->define('duration')
            ->default(function (OptionsResolver $durationResolver) {
                $durationResolver->define('value')
                    ->allowedTypes('int')
                    ->required()
                    ->ignoreUndefined();
            })
            ->ignoreUndefined();
    }

    /**
     * @param  mixed  $source
     *
     * @return ResponseData
     */
    public static function fromResponseJson(mixed $source): ResponseData
    {
        return new self($source);
    }
}
