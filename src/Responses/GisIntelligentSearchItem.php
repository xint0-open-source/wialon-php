<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Xint0\WialonPhp\Responses;

use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GisIntelligentSearchItem
{
    public readonly string $city;
    public readonly string $country;
    public readonly string $formatted_path;
    public readonly string $house;
    public readonly float $latitude;
    public readonly float $longitude;
    public readonly string $map;
    public readonly ?float $probability;
    public readonly string $region;
    public readonly string $street;

    /** @var OptionsResolver[] $resolvers */
    private static array $resolvers = [];

    /**
     * @param  array{
     *     city: string,
     *     country: string,
     *     formatted_path: string,
     *     house: string,
     *     latitude: float,
     *     longitude: float,
     *     map: string,
     *     probability?: float,
     *     region: string,
     *     street: string
     * }  $data
     *
     * @throws InvalidOptionsException When option value has wrong type.
     * @throws InvalidOptionsException When latitude value is outside range -90 to 90.
     * @throws InvalidOptionsException When longitude value is outside range -180 to 180.
     * @throws MissingOptionsException When required value is missing.
     */
    public function __construct(array $data)
    {
        $resolver = self::getResolver(get_class($this));
        $options = $resolver->resolve($data);
        $this->city = $options['city'];
        $this->country = $options['country'];
        $this->formatted_path = $options['formatted_path'];
        $this->house = $options['house'];
        $this->latitude = $options['latitude'];
        $this->longitude = $options['longitude'];
        $this->map = $options['map'];
        $this->probability = $options['probability'];
        $this->region = $options['region'];
        $this->street = $options['street'];
    }

    /**
     * @param  class-string<GisIntelligentSearchItem>  $class
     */
    private static function getResolver(string $class): OptionsResolver
    {
        if (! isset(self::$resolvers[$class])) {
            self::$resolvers[$class] = new OptionsResolver();
            static::configureOptions(self::$resolvers[$class]);
        }

        return self::$resolvers[$class];
    }

    public static function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->define('city')
            ->required()
            ->allowedTypes('string');
        $resolver->define('country')
            ->required()
            ->allowedTypes('string');
        $resolver->define('formatted_path')
            ->required()
            ->allowedTypes('string');
        $resolver->define('house')
            ->required()
            ->allowedTypes('string');
        $resolver->define('map')
            ->required()
            ->allowedTypes('string');
        $resolver->define('region')
            ->required()
            ->allowedTypes('string');
        $resolver->define('street')
            ->required()
            ->allowedTypes('string');
        $resolver->define('longitude')
            ->required()
            ->allowedTypes('float')
            ->allowedValues(fn (float $v) => $v >= -180.00000000 && $v <= 180.00000000);
        $resolver->define('latitude')
            ->required()
            ->allowedTypes('float')
            ->allowedValues(fn (float $v) => $v >= -90.00000000 && $v <= 90.00000000);
        $resolver->define('probability')
            ->default(null)
            ->allowedTypes('null', 'float');
        $resolver->setIgnoreUndefined();
    }
}
