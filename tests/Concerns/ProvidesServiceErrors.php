<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Concerns;

trait ProvidesServiceErrors
{
    public static function serviceErrorProvider(): array
    {
        return [
            'Invalid session' => [
                'responseContents' => '{"error":1}',
                'expectedCode' => 1,
                'expectedMessage' => 'Service error: Invalid session.',
            ],
            'Invalid service name' => [
                'responseContents' => '{"error":2}',
                'expectedCode' => 2,
                'expectedMessage' => 'Service error: Invalid service name.',
            ],
            'Invalid result' => [
                'responseContents' => '{"error":3}',
                'expectedCode' => 3,
                'expectedMessage' => 'Service error: Invalid result.',
            ],
            'Invalid input' => [
                'responseContents' => '{"error":4}',
                'expectedCode' => 4,
                'expectedMessage' => 'Service error: Invalid input.',
            ],
            'Error performing request' => [
                'responseContents' => '{"error":5}',
                'expectedCode' => 5,
                'expectedMessage' => 'Service error: Error performing request.',
            ],
            'Unknown error' => [
                'responseContents' => '{"error":6}',
                'expectedCode' => 6,
                'expectedMessage' => 'Service error: Unknown error.',
            ],
            'Access denied' => [
                'responseContents' => '{"error":7}',
                'expectedCode' => 7,
                'expectedMessage' => 'Service error: Access denied.',
            ],
            'Invalid user name or password' => [
                'responseContents' => '{"error":8}',
                'expectedCode' => 8,
                'expectedMessage' => 'Service error: Invalid user name or password.',
            ],
            'Authorization server is unavailable' => [
                'responseContents' => '{"error":9}',
                'expectedCode' => 9,
                'expectedMessage' => 'Service error: Authorization server is unavailable.',
            ],
            'Reached limit of concurrent requests.' => [
                'responseContents' => '{"error":10}',
                'expectedCode' => 10,
                'expectedMessage' => 'Service error: Reached limit of concurrent requests.',
            ],
            'Password reset error' => [
                'responseContents' => '{"error":11}',
                'expectedCode' => 11,
                'expectedMessage' => 'Service error: Password reset error.',
            ],
            'Billing error' => [
                'responseContents' => '{"error":14}',
                'expectedCode' => 14,
                'expectedMessage' => 'Service error: Billing error.',
            ],
            'No messages for selected interval' => [
                'responseContents' => '{"error":1001}',
                'expectedCode' => 1001,
                'expectedMessage' => 'Service error: No messages for selected interval.',
            ],
            'Item already exists or cannot be created' => [
                'responseContents' => '{"error":1002}',
                'expectedCode' => 1002,
                'expectedMessage' => 'Service error: Item with same properties already exists or cannot be created according to billing restrictions.',
            ],
            'Only one request is allowed at the moment' => [
                'responseContents' => '{"error":1003}',
                'expectedCode' => 1003,
                'expectedMessage' => 'Service error: Only one request is allowed at the moment.',
            ],
            'Limit of messages has been exceeded' => [
                'responseContents' => '{"error":1004}',
                'expectedCode' => 1004,
                'expectedMessage' => 'Service error: Limit of messages has been exceeded.',
            ],
            'Execution time has exceeded the limit.' => [
                'responseContents' => '{"error":1005}',
                'expectedCode' => 1005,
                'expectedMessage' => 'Service error: Execution time has exceeded the limit.',
            ],
            'Exceeding the limit of attempts to enter a two-factor authorization code' => [
                'responseContents' => '{"error":1006}',
                'expectedCode' => 1006,
                'expectedMessage' => 'Service error: Exceeding the limit of attempts to enter a two-factor authorization code.',
            ],
            'Your IP has changed or session has expired' => [
                'responseContents' => '{"error":1011}',
                'expectedCode' => 1011,
                'expectedMessage' => 'Service error: Your IP has changed or session has expired.',
            ],
            'Not possible to transfer unit to this account' => [
                'responseContents' => '{"error":2006}',
                'expectedCode' => 2006,
                'expectedMessage' => 'Service error: Not possible to transfer unit to this account.',
            ],
            'User does not have access to unit (due to transferring to new account)' => [
                'responseContents' => '{"error":2008}',
                'expectedCode' => 2008,
                'expectedMessage' => 'Service error: User does not have access to unit (due to transferring to new account).',
            ],
            'Selected user is a creator for some system objects, thus cannot be bound to new account' => [
                'responseContents' => '{"error":2014}',
                'expectedCode' => 2014,
                'expectedMessage' => 'Service error: Selected user is a creator for some system objects, thus cannot be bound to new account.',
            ],
            'Cannot delete sensor because it is used in another sensor or advanced properties of unit' => [
                'responseContents' => '{"error":2015}',
                'expectedCode' => 2015,
                'expectedMessage' => 'Service error: Cannot delete sensor because it is used in another sensor or advanced properties of unit.',
            ],
            'Unknown error code' => [
                'responseContents' => '{"error":9999}',
                'expectedCode' => 9999,
                'expectedMessage' => 'Service error: Unknown service error code 9999.',
            ],
        ];
    }

    public static function unexpectedHttpCodeProvider(): iterable
    {
        for ($code = 100; $code <= 600; $code++) {
            if ($code === 200) {
                continue;
            }
            $exceptionMessage = "Unexpected code: $code.";
            $expectedCode = 30000 + $code;
            if ($code >= 400 && $code < 500) {
                $exceptionMessage = "Client error code: $code.";
                $expectedCode = 10000 + $code;
            }
            if ($code >= 500 && $code < 600) {
                $exceptionMessage = "Server error code: $code.";
                $expectedCode = 20000 + $code;
            }
            yield "Code $code" => [$code, $exceptionMessage, $expectedCode];
        }
    }
}