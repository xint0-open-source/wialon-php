<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Tests\Unit\Requests;

use PHPUnit\Framework\Attributes\CoversClass;
use Tests\TestCase;
use Xint0\WialonPhp\Requests\GisGetRouteParameters;

#[CoversClass(GisGetRouteParameters::class)]
class GisGetRouteParametersTest extends TestCase
{
    public function test_new_instance_has_expected_property_values(): void
    {
        $expected_latitude_one = 31.7132597;
        $expected_longitude_one = -106.3956615;
        $expected_latitude_two = 31.8120716;
        $expected_longitude_two = -106.3971567;
        $expected_flags = 1;
        $expected_user_id = 5794;
        $sut = new GisGetRouteParameters(
            $expected_latitude_one,
            $expected_longitude_one,
            $expected_latitude_two,
            $expected_longitude_two,
            $expected_flags,
            $expected_user_id,
        );
        $this->assertSame($expected_latitude_one, $sut->latitudeOne);
        $this->assertSame($expected_longitude_one, $sut->longitudeOne);
        $this->assertSame($expected_latitude_two, $sut->latitudeTwo);
        $this->assertSame($expected_longitude_two, $sut->longitudeTwo);
        $this->assertSame($expected_flags, $sut->flags);
        $this->assertSame($expected_user_id, $sut->userId);
    }

    public function test_url_encode_method_returns_expected_value(): void
    {
        $expected_value = 'lat1=31.7132597&lon1=-106.3956615&lat2=31.8120716&lon2=-106.3971567&flags=1&uid=5794';
        $sut = new GisGetRouteParameters(31.7132597, -106.3956615, 31.8120716, -106.3971567, 1, 5794);
        $this->assertSame($expected_value, $sut->urlEncode());
    }
}
