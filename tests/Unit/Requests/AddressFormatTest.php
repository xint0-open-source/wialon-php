<?php

declare(strict_types=1);

namespace Tests\Unit\Requests;

use DomainException;
use LengthException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Xint0\WialonPhp\Requests\AddressFormat;
use PHPUnit\Framework\TestCase;

#[CoversClass(AddressFormat::class)]
class AddressFormatTest extends TestCase
{
    public static function invalidFormatProvider(): array
    {
        return [
            'Invalid character' => [
                'Ñ',
                DomainException::class,
                'The specified format "Ñ" is invalid.',
            ],
            'More than 5 valid digits' => [
                '123454',
                LengthException::class,
                'The specified format length (6) is not between 1 and 5.',
            ],
            '5 invalid digits' => [
                '67896',
                DomainException::class,
                'The specified format "67896" is invalid.',
            ],
            'Empty string' => [
                '',
                LengthException::class,
                'The specified format length (0) is not between 1 and 5.',
            ],
        ];
    }

    #[DataProvider('invalidFormatProvider')]
    public function test_constructor_throws_out_of_range_exception_with_invalid_format(string $format, string $expected_exception_class, string $expected_exception_message): void
    {
        $this->expectException($expected_exception_class);
        $this->expectExceptionMessage($expected_exception_message);
        new AddressFormat($format);
    }

    public static function intValueProvider(): array
    {
        return [
            'country' => [
                '1',
                268435456,
            ],
            'region, country' => [
                '21',
                570425344,
            ],
            'city, region, country' => [
                '321',
                876609536,
            ],
            'street, city, region, country' => [
                '4321',
                1183318016,
            ],
            'house, street, city, region, country' => [
                '54321',
                1490092032,
            ],
        ];
    }

    #[DataProvider('intValueProvider')]
    public function test_instance_has_expected_int_value(string $format, int $expected_value): void
    {
        $sut = new AddressFormat($format);
        $this->assertSame($expected_value, $sut->intValue);
    }

    public static function normalizedFormatProvider(): array
    {
        return [
            'country' => [
                '1',
                '10000',
            ],
            'region, country' => [
                '21',
                '21000',
            ],
            'city, region, country' => [
                '321',
                '32100',
            ],
            'street, city, region, country' => [
                '4321',
                '43210',
            ],
            'house, street, city, region, country' => [
                '54321',
                '54321',
            ],
        ];
    }

    #[DataProvider('normalizedFormatProvider')]
    public function test_instance_has_expected_normalized_format_value(string $format, string $expected_value): void
    {
        $sut = new AddressFormat($format);
        $this->assertSame($expected_value, $sut->normalizedFormat);
    }
}
