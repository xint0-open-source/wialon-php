<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2023 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit\Requests;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Xint0\WialonPhp\Requests\AddressFormat;
use Xint0\WialonPhp\Requests\GisGeocodeRequestParameters;
use PHPUnit\Framework\TestCase;

#[CoversClass(GisGeocodeRequestParameters::class)]
#[UsesClass(AddressFormat::class)]
class GisGeocodeRequestParametersTest extends TestCase
{
    public function test_new_instance_has_expected_properties(): void
    {
        $expected_address_format = new AddressFormat('321');
        $expected_latitude = 30.6141861;
        $expected_longitude = -111.004287;
        $expected_user_id = 1234;
        $sut = new GisGeocodeRequestParameters($expected_longitude, $expected_latitude, $expected_address_format, $expected_user_id);
        $this->assertSame($expected_address_format, $sut->addressFormat);
        $this->assertSame($expected_latitude, $sut->latitude);
        $this->assertSame($expected_longitude, $sut->longitude);
        $this->assertSame($expected_user_id, $sut->user_id);
    }

    public function test_url_encode_returns_expected_value(): void
    {
        $expected_address_format = new AddressFormat('321');
        $expected_latitude = 30.6141861;
        $expected_longitude = -111.004287;
        $expected_user_id = 1234;
        $expected_value = 'coords=' . urlencode(json_encode([['lon' => $expected_longitude, 'lat' => $expected_latitude]])) .
            '&flags=' . urlencode((string)$expected_address_format->intValue) .
            '&uid=' . urlencode((string)$expected_user_id);
        $sut = new GisGeocodeRequestParameters($expected_longitude, $expected_latitude, $expected_address_format, $expected_user_id);
        $this->assertSame($expected_value, $sut->urlEncode());
    }
}
