<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2023 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit\Requests;

use PHPUnit\Framework\Attributes\CoversClass;
use Xint0\WialonPhp\Requests\GisIntelligentSearchParameters;
use Tests\TestCase;

#[CoversClass(GisIntelligentSearchParameters::class)]
class GisIntelligentSearchParametersTest extends TestCase
{
    public function test_new_instance_without_optional_parameters_has_expected_property_values(): void
    {
        $sut = new GisIntelligentSearchParameters('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico', 1234);
        $this->assertSame('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico', $sut->phrase);
        $this->assertSame(1234, $sut->userId);
        $this->assertSame(10, $sut->count);
        $this->assertSame(0, $sut->indexFrom);
    }

    public function test_new_instance_with_optional_parameters_has_expected_property_values(): void
    {
        $sut = new GisIntelligentSearchParameters('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico', 1234, 5, 5);
        $this->assertSame('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico', $sut->phrase);
        $this->assertSame(1234, $sut->userId);
        $this->assertSame(5, $sut->count);
        $this->assertSame(5, $sut->indexFrom);
    }

    public function test_url_encode_method_returns_expected_value_without_optional_parameters(): void
    {
        $expected_value = 'phrase=' . urlencode('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico') .
            '&count=10&uid=1234';
        $sut = new GisIntelligentSearchParameters('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico', 1234);
        $this->assertSame($expected_value, $sut->urlEncode());
    }

    public function test_url_encode_method_returns_expected_value_with_optional_parameters(): void
    {
        $expected_value = 'phrase=' . urlencode('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico') .
            '&count=5&indexFrom=5&uid=1234';
        $sut = new GisIntelligentSearchParameters('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32472, Mexico', 1234, 5, 5);
        $this->assertSame($expected_value, $sut->urlEncode());
    }
}
