<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit\Wialon;

use Exception;
use GuzzleHttp\Psr7\Utils;
use Http\Client\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\TransferException;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Throwable;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

#[CoversClass(Wialon::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(WialonException::class)]
class UnitByNameTest extends TestCase
{
    use ProvidesServiceErrors;

    /**
     * @uses \Xint0\WialonPhp\Factories\RequestFactory
     */
    public function test_makes_expected_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $session_id = json_decode($loginResponseBody, true)['eid'];
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            $params = [
                'spec' => [
                    'itemsType' => 'avl_unit',
                    'propName' => 'sys_name',
                    'propValueMask' => 'F022',
                    'sortType' => '',
                    'propType' => '',
                    'or_logic' => false,
                ],
                'force' => 1,
                'flags' => 0x411,
                'from' => 0,
                'to' => 0,
            ];
            $expectedBody = 'svc=' . urlencode('core/search_items') .
                '&params='  . urlencode(json_encode($params)) .
                '&sid=' . urlencode($session_id);
            $this->assertSame($expectedBody, (string)$request->getBody());
            $unitByNameResponseBody = $this->getFixtureContents('unit_by_name_response.json');
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($unitByNameResponseBody));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        $sut->unitByName('F022');
        $this->assertCount(2, $mockClient->getRequests());
    }

    public function test_throws_wialon_exception_when_session_id_is_null(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $this->expectException('\Xint0\WialonPhp\WialonException');
        $this->expectExceptionMessage('Client does not have session ID.');
        $this->expectExceptionCode(9001);

        $sut->unitByName('F022');
    }

    public function test_returns_expected_value_on_success(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $unitByNameResponseBody = $this->getFixtureContents('unit_by_name_response.json');
        $expected = json_decode($unitByNameResponseBody, true);
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($unitByNameResponseBody) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($unitByNameResponseBody));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');

        $actual = $sut->unitByName('F022');

        $this->assertSame($expected, $actual);
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_throws_wialon_exception_on_error_response(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($responseContents) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($responseContents));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        $sut->unitByName('F022');
    }

    public function test_throws_expected_wialon_exception_when_transfer_exception_is_thrown(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $previousException = new TransferException('Could not send request.');
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($previousException) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw $previousException;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->unitByName('F022');
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertSame($previousException, $exception->getPrevious());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    public function test_throws_expected_wialon_exception_when_request_exception_is_thrown(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher(null, 'track.agilogistics.net');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new RequestException('Invalid request', $request);
        });

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->unitByName('F022');
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(RequestException::class, $exception->getPrevious());
            $this->assertSame('Invalid request', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    public function test_throws_expected_wialon_exception_when_network_exception_is_thrown(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher(null, 'track.agilogistics.net');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new NetworkException('Network failure', $request);
        });

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->unitByName('F022');
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(NetworkException::class, $exception->getPrevious());
            $this->assertSame('Network failure', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_throws_wialon_exception_when_http_status_code_is_not_success(int $http_status, string $expected_message, int $expected_code): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher(null, 'track.agilogistics.net');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($http_status) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn($http_status);
            return $stubResponse;
        });

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->unitByName('F022');
        } catch (WialonException $exception) {
            $this->assertSame($expected_message, $exception->getMessage());
            $this->assertSame($expected_code, $exception->getCode());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }
}
