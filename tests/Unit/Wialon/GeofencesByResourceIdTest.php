<?php

namespace Tests\Unit\Wialon;

use Exception;
use GuzzleHttp\Psr7\Utils;
use Http\Client\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\TransferException;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Throwable;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

#[CoversClass(Wialon::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(WialonException::class)]
class GeofencesByResourceIdTest extends TestCase
{
    use ProvidesServiceErrors;

    public function test_makes_expected_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $loginResponseBody = $this->getFixtureContents('login_response.json');
        $session_id = json_decode($loginResponseBody, true)['eid'];
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($loginResponseBody) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('resource_zone_data_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            throw new Exception('Unexpected request');
        });
        $resource_id = 1234;

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        $sut->geofencesByResourceId($resource_id);
        $requests = $mockClient->getRequests();
        $this->assertCount(2, $requests);
        $getZoneDataRequest = $requests[1];
        $expectedBody = 'svc=' . urlencode('resource/get_zone_data') .
            '&params=' . urlencode(json_encode([
                'itemId' => $resource_id,
                'flags' => 0x10,
            ])) .
            '&sid=' . urlencode($session_id);
        $this->assertSame($expectedBody, (string)$getZoneDataRequest->getBody());
    }

    public function test_makes_expected_request_when_ids_exists(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $loginResponseBody = $this->getFixtureContents('login_response.json');
        $session_id = json_decode($loginResponseBody, true)['eid'];
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($loginResponseBody) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('resource_zone_data_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            throw new Exception('Unexpected request');
        });
        $resource_id = 1234;

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');

        $geofences_ids = [99, 100];
        $flags = 0x18;
        $sut->geofencesByResourceId($resource_id, $geofences_ids, $flags);
        $requests = $mockClient->getRequests();
        $this->assertCount(2, $requests);
        $getZoneDataRequest = $requests[1];
        $expectedBody = 'svc=' . urlencode('resource/get_zone_data') .
            '&params=' . urlencode(json_encode([
                'itemId' => $resource_id,
                'flags' => $flags,
                'col' => $geofences_ids,
            ])) .
            '&sid=' . urlencode($session_id);
        $this->assertSame($expectedBody, (string)$getZoneDataRequest->getBody());
    }

    public function test_throws_wialon_exception_when_session_id_is_null(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $this->expectException(WialonException::class);
        $this->expectExceptionMessage('Client does not have session ID.');
        $this->expectExceptionCode(9001);

        $sut->geofencesByResourceId(1234);
    }

    public function test_returns_expected_value_on_success(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $resourceZoneDataResponseBody = $this->getFixtureContents('resource_zone_data_response.json');
        $expected = json_decode($resourceZoneDataResponseBody, true);
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($resourceZoneDataResponseBody) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($resourceZoneDataResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');

        $actual = $sut->geofencesByResourceId(1234);

        $this->assertSame($expected, $actual);
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_throws_wialon_exception_on_error_response(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($responseContents) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($responseContents));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            throw new Exception('Unexpected request');
        });

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        $sut->geofencesByResourceId(1234);
    }

    public function test_throws_wialon_exception_when_client_throws_transfer_exception(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $expectedPreviousException = new TransferException('Could not send request.');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($expectedPreviousException) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                throw $expectedPreviousException;
            }
            throw new Exception('Unexpected request');
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        try {
            $sut->geofencesByResourceId(1234);
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertSame($expectedPreviousException, $exception->getPrevious());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    public function test_throws_expected_wialon_exception_when_client_throws_request_exception(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                throw new RequestException('Invalid request', $request);
            }
            throw new Exception('Unexpected request');
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        try {
            $sut->geofencesByResourceId(1234);
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(RequestException::class, $exception->getPrevious());
            $this->assertSame('Invalid request', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    public function test_throws_expected_wialon_exception_when_client_throws_network_exception(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                throw new NetworkException('Network failure', $request);
            }
            throw new Exception('Unexpected request');
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        try {
            $sut->geofencesByResourceId(1234);
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(NetworkException::class, $exception->getPrevious());
            $this->assertSame('Network failure', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_throws_wialon_exception_when_http_status_code_is_not_success(int $http_status, string $expected_message, int $expected_code): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($http_status) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Psr17FactoryDiscovery::findStreamFactory()->createStream($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            if (str_contains($requestBody, 'svc=' . urlencode('resource/get_zone_data'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getStatusCode')->willReturn($http_status);
                return $stubResponse;
            }
            throw new Exception('Unexpected request');
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        try {
            $sut->geofencesByResourceId(1234);
        } catch (WialonException $exception) {
            $this->assertSame($expected_message, $exception->getMessage());
            $this->assertSame($expected_code, $exception->getCode());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }
}
