<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2024. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Tests\Unit\Wialon;

use Exception;
use GuzzleHttp\Psr7\Utils;
use Http\Client\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\TransferException;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Requests\GisGetRouteParameters;
use Xint0\WialonPhp\Responses\GisRoute;
use Xint0\WialonPhp\Responses\WialonResponse;
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

#[CoversClass(Wialon::class)]
#[UsesClass(WialonException::class)]
#[UsesClass(WialonResponse::class)]
#[UsesClass(GisRoute::class)]
#[UsesClass(GisGetRouteParameters::class)]
#[UsesClass(RequestFactory::class)]
class GisGetRouteTest extends TestCase
{
    use ProvidesServiceErrors;

    public function test_makes_expected_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $expected_latitude_one = 31.7132597;
        $expected_longitude_one = -106.3956615;
        $expected_latitude_two = 31.8120716;
        $expected_longitude_two = -106.3971567;
        $expected_flags = 1;
        $expected_user_id = 5794;
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn(200);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            return $stubResponse;
        });
        $gisRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $expected_request_body = 'lat1=' . $expected_latitude_one . '&lon1=' . $expected_longitude_one . '&lat2=' . $expected_latitude_two . '&lon2=' . $expected_longitude_two . '&flags=' . $expected_flags . '&uid=' . $expected_user_id;
        $mockClient->on($gisRouteRequestMatcher, function (RequestInterface $request) use ($expected_request_body) {
            $this->assertSame($expected_request_body, $request->getBody()->getContents());
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn(200);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('gis_route_response.json')));
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $sut->gisGetRoute($expected_latitude_one, $expected_longitude_one, $expected_latitude_two, $expected_longitude_two);

        $this->assertCount(2, $mockClient->getRequests());
    }

    public function test_throws_wialon_exception_when_session_id_is_null(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request!'));
        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $this->expectException(WialonException::class);
        $this->expectExceptionMessage('Client does not have session ID.');
        $this->expectExceptionCode(9001);

        $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_throws_wialon_exception_on_service_error_response(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGetRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGetRouteRequestMatcher, function () use ($responseContents) {
            $stubReponse = $this->createStub(ResponseInterface::class);
            $stubReponse->method('getBody')->willReturn(Utils::streamFor($responseContents));
            $stubReponse->method('getStatusCode')->willReturn(200);
            return $stubReponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);

        $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);
    }

    public function test_throws_wialon_exception_when_transfer_exception_is_thrown_while_processing_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGetRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $previousException = new TransferException('Could not send request.');
        $mockClient->on($gisGetRouteRequestMatcher, function () use ($previousException) {
            throw $previousException;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9000);
        $this->expectExceptionMessage('Service request failed.');

        try {
            $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);
        } catch (WialonException $e) {
            $this->assertSame($previousException, $e->getPrevious());
            throw $e;
        } catch (Exception $exception) {
            $this->fail(sprintf('Unexpected exception [%s]: %s', get_class($exception), $exception->getMessage()));
        }
    }

    public function test_throws_wialon_exception_when_request_exception_is_thrown_while_processing_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGetRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGetRouteRequestMatcher, function (RequestInterface $request) {
            throw new RequestException('Invalid request', $request);
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9000);
        $this->expectExceptionMessage('Service request failed.');

        try {
            $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);
        } catch (WialonException $e) {
            $this->assertInstanceOf(RequestException::class, $e->getPrevious());
            $this->assertSame('Invalid request', $e->getPrevious()->getMessage());
            throw $e;
        } catch (Exception $exception) {
            $this->fail(sprintf('Unexpected exception [%s]: %s', get_class($exception), $exception->getMessage()));
        }
    }

    public function test_throws_wialon_exception_when_network_exception_is_thrown_while_processing_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGetRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGetRouteRequestMatcher, function (RequestInterface $request) {
            throw new NetworkException('Network failure', $request);
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9000);
        $this->expectExceptionMessage('Service request failed.');

        try {
            $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);
        } catch (WialonException $e) {
            $this->assertInstanceOf(NetworkException::class, $e->getPrevious());
            $this->assertSame('Network failure', $e->getPrevious()->getMessage());
            throw $e;
        } catch (Exception $exception) {
            $this->fail(sprintf('Unexpected exception [%s]: %s', get_class($exception), $exception->getMessage()));
        }
    }

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_throws_wialon_exception_when_http_status_code_is_not_success(int $http_status, string $expected_message, int $expected_code): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGetRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGetRouteRequestMatcher, function (RequestInterface $request) use ($http_status) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn($http_status);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expected_code);
        $this->expectExceptionMessage($expected_message);

        $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);
    }

    public function test_returns_expected_value_on_success(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn(200);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            return $stubResponse;
        });
        $gisRouteRequestMatcher = new RequestMatcher('gis_get_route', 'track.agilogistics.net', 'POST', 'https');
        $responseBody = $this->getFixtureContents('gis_route_response.json');
        $expectedData = json_decode($responseBody);
        $mockClient->on($gisRouteRequestMatcher, function () use ($responseBody) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn(200);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($responseBody));
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $actual = $sut->gisGetRoute(31.7132597, -106.3956615, 31.8120716, -106.3971567);

        $this->assertSame($expectedData->status, $actual->data()->status);
        $this->assertSame($expectedData->points, $actual->data()->points);
        $this->assertSame($expectedData->distance->value, $actual->data()->distance);
        $this->assertSame($expectedData->duration->value, $actual->data()->duration);
    }
}
