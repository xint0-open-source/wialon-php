<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit\Wialon;

use Exception;
use GuzzleHttp\Psr7\Utils;
use Http\Client\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\TransferException;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Throwable;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Requests\AddressFormat;
use Xint0\WialonPhp\Requests\GisGeocodeRequestParameters;
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

#[CoversClass(Wialon::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(AddressFormat::class)]
#[UsesClass(GisGeocodeRequestParameters::class)]
#[UsesClass(WialonException::class)]
class GetLocationsTest extends TestCase
{
    use ProvidesServiceErrors;

    public function test_makes_expected_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function (RequestInterface $request) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')
                ->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGeocodeRequestMatcher, function (RequestInterface $request) {
            $expectedBody = 'coords=' . urlencode('[{"lon":-111.004287,"lat":30.6141861}]') .
                '&flags=876609536&uid=5794';
            $this->assertSame($expectedBody, (string)$request->getBody());
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor('["Magdalena, Sonora, Mexico"]'));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new \Exception('Unexpected request!'));
        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $sut->getLocations(-111.004287, 30.6141861, '32100');
    }

    public function test_throws_wialon_exception_when_session_id_is_null(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request!'));
        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $this->expectException('\\Xint0\\WialonPhp\\WialonException');
        $this->expectExceptionMessage('Client does not have session ID.');
        $this->expectExceptionCode(9001);
        $sut->getLocations(-111.004287, 30.6141861, '32100');
    }

    public function test_returns_expected_value_on_success(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function (RequestInterface $request) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')
                ->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $responseBody = '["Magdalena, Sonora, Mexico"]';
        $expected = json_decode($responseBody, true);
        $mockClient->on($gisGeocodeRequestMatcher, function () use ($responseBody) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($responseBody));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new \Exception('Unexpected request!'));
        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $actual = $sut->getLocations(-111.004287, 30.6141861, '32100');

        $this->assertSame($expected, $actual);
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_throws_wialon_exception_on_error_response(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $baseUri = 'https://track.agilogistics.net/';
        $token = 'test_token';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }
            throw new Exception('Unexpected request!');
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGeocodeRequestMatcher, function () use ($responseContents) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($responseContents));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $sut->getLocations(-111.004287, 30.6141861, '32100');
    }

    public function test_throws_expected_wialon_exception_when_transfer_exception_is_thrown(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $previousException = new TransferException('Could not send request.');
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request!');
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGeocodeRequestMatcher, function () use ($previousException) {
            throw $previousException;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->getLocations(-111.004287, 30.6141861, '32100');
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertSame($previousException, $exception->getPrevious());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    public function test_throws_expected_wialon_exception_when_request_exception_is_thrown(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request!');
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGeocodeRequestMatcher, function (RequestInterface $request) {
            throw new RequestException('Invalid request', $request);
        });

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->getLocations(-111.004287, 30.6141861, '32100');
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(RequestException::class, $exception->getPrevious());
            $this->assertSame('Invalid request', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    public function test_throws_expected_wialon_exception_when_network_exception_is_thrown(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request!');
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGeocodeRequestMatcher, function (RequestInterface $request) {
            throw new NetworkException('Network failure', $request);
        });

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->getLocations(-111.004287, 30.6141861, '32100');
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(NetworkException::class, $exception->getPrevious());
            $this->assertSame('Network failure', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_throws_wialon_exception_when_http_status_is_not_success_code(int $code, string $expected_message, int $expected_exception_code): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $loginResponseBody = $this->getFixtureContents('login_response.json');
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request!');
        });
        $gisGeocodeRequestMatcher = new RequestMatcher('gis_geocode', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisGeocodeRequestMatcher, function (RequestInterface $request) use ($code) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn($code);
            return $stubResponse;
        });

        $sut = new Wialon($baseUri, $mockClient);
        try {
            $sut->login('test_token');
            $sut->getLocations(-111.004287, 30.6141861, '32100');
        } catch (WialonException $exception) {
            $this->assertSame($expected_message, $exception->getMessage());
            $this->assertSame($expected_exception_code, $exception->getCode());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail("Unexpected exception [$exceptionClass]: {$exception->getMessage()}");
        }
    }
}
