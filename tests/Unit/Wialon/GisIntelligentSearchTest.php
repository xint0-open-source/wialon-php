<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Tests\Unit\Wialon;

use Exception;
use GuzzleHttp\Psr7\Utils;
use Http\Client\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\TransferException;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Requests\GisIntelligentSearchParameters;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItem;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItems;
use Xint0\WialonPhp\Responses\WialonResponse;
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

#[CoversClass(Wialon::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(GisIntelligentSearchItem::class)]
#[UsesClass(GisIntelligentSearchItems::class)]
#[UsesClass(GisIntelligentSearchParameters::class)]
#[UsesClass(WialonException::class)]
#[UsesClass(WialonResponse::class)]
class GisIntelligentSearchTest extends TestCase
{
    use ProvidesServiceErrors;

    public function test_makes_expected_request_without_optional_parameters(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $phrase = 'Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function (RequestInterface $request) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')
                ->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchMatcher, function (RequestInterface $request) use ($phrase) {
            $expectedBody = 'phrase=' . urlencode($phrase) . '&count=10&uid=5794';
            $this->assertSame($expectedBody, (string)$request->getBody());
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('gis_intelligent_search_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $sut->gisIntelligentSearch($phrase);
    }

    public function test_makes_expected_request_with_optional_count_parameter(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $phrase = 'phrase';
        $count = 5;
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')
                ->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchMatcher, function (RequestInterface $request) use ($phrase, $count) {
            $expectedBody = 'phrase=' . urlencode($phrase) . "&count=$count&uid=5794";
            $actualBody = (string)$request->getBody();
            $this->assertSame($expectedBody, $actualBody);
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor('[]'));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $sut->gisIntelligentSearch($phrase, $count);
        $this->assertCount(2, $mockClient->getRequests());
    }

    public function test_makes_expected_request_with_optional_count_and_index_from_parameters(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $phrase = 'phrase';
        $count = 5;
        $index_from = 6;
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')
                ->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchMatcher, function (RequestInterface $request) use ($phrase, $count, $index_from) {
            $expectedBody = 'phrase=' . urlencode($phrase) . "&count=$count&indexFrom=$index_from&uid=5794";
            $actualBody = (string)$request->getBody();
            $this->assertSame($expectedBody, $actualBody);
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor('[]'));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $sut->gisIntelligentSearch($phrase, $count, $index_from);
        $this->assertCount(2, $mockClient->getRequests());
    }

    public function test_throws_wialon_exception_when_session_id_is_null(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request!'));
        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $this->expectException(WialonException::class);
        $this->expectExceptionMessage('Client does not have session ID.');
        $this->expectExceptionCode(9001);
        $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
    }

    public function test_returns_expected_value_on_success(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchRequestMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchRequestMatcher, function () {
            $stubReponse = $this->createStub(ResponseInterface::class);
            $stubReponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('gis_intelligent_search_response.json')));
            $stubReponse->method('getStatusCode')->willReturn(200);
            return $stubReponse;
        });
        $expectedItems = new GisIntelligentSearchItems(...array_map(fn ($item) => new GisIntelligentSearchItem($item), json_decode($this->getFixtureContents('gis_intelligent_search_items.json'), true)));
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);
        $actual = $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
        $this->assertEquals($expectedItems, $actual->data());
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_throws_wialon_exception_on_service_error_response(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchRequestMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchRequestMatcher, function () use ($responseContents) {
            $stubReponse = $this->createStub(ResponseInterface::class);
            $stubReponse->method('getBody')->willReturn(Utils::streamFor($responseContents));
            $stubReponse->method('getStatusCode')->willReturn(200);
            return $stubReponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);
        $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
    }

    public function test_throws_wialon_exception_when_transfer_exception_is_thrown_while_processing_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchRequestMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $previousException = new TransferException('Could not send request.');
        $mockClient->on($gisIntelligentSearchRequestMatcher, function () use ($previousException) {
            throw $previousException;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9000);
        $this->expectExceptionMessage('Service request failed.');
        try {
            $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
        } catch (WialonException $exception) {
            $this->assertSame($previousException, $exception->getPrevious());
            throw $exception;
        }
    }

    public function test_throws_wialon_exception_when_request_exception_is_thrown_while_processing_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchRequestMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchRequestMatcher, function (RequestInterface $request) {
            throw new RequestException('Invalid request', $request);
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9000);
        $this->expectExceptionMessage('Service request failed.');
        try {
            $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
        } catch (WialonException $exception) {
            $this->assertInstanceOf(RequestException::class, $exception->getPrevious());
            $this->assertSame('Invalid request', $exception->getPrevious()->getMessage());
            throw $exception;
        }
    }

    public function test_throws_wialon_exception_when_network_exception_is_thrown_while_processing_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchRequestMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchRequestMatcher, function (RequestInterface $request) {
            throw new NetworkException('Network failure', $request);
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9000);
        $this->expectExceptionMessage('Service request failed.');

        try {
            $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
        } catch (WialonException $exception) {
            $this->assertInstanceOf(NetworkException::class, $exception->getPrevious());
            $this->assertSame('Network failure', $exception->getPrevious()->getMessage());
            throw $exception;
        }
    }

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_throws_wialon_exception_when_https_status_is_not_success_code(int $code, string $expectedMessage, int $expectedCode): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $token = 'test_token';
        $mockClient = new Client();
        $loginRequestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($loginRequestMatcher, function () {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
            $stubResponse->method('getStatusCode')->willReturn(200);
            return $stubResponse;
        });
        $gisIntelligentSearchRequestMatcher = new RequestMatcher('gis_searchintelli', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($gisIntelligentSearchRequestMatcher, function () use ($code) {
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getBody')->willReturn(Utils::streamFor('Error message'));
            $stubResponse->method('getStatusCode')->willReturn($code);
            return $stubResponse;
        });
        $mockClient->setDefaultException(new Exception('Unexpected request!'));

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login($token);

        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);

        $sut->gisIntelligentSearch('Avenida Campos Elíseos 9050, Juárez, Chihuahua 32472, México');
    }
}
