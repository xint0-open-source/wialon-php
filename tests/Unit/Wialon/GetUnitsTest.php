<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2023 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

namespace Tests\Unit\Wialon;

use Exception;
use GuzzleHttp\Psr7\Utils;
use Http\Client\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\TransferException;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Throwable;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Wialon;
use Xint0\WialonPhp\WialonException;

#[CoversClass(Wialon::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(WialonException::class)]
class GetUnitsTest extends TestCase
{
    use ProvidesServiceErrors;

    public function test_makes_expected_request(): void
    {
        $baseUri = 'https://track.agilogistics.net';
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $loginResponseBody = $this->getFixtureContents('login_response.json');
        $session_id = json_decode($loginResponseBody, true)['eid'];
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($loginResponseBody) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('get_units_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon($baseUri, $mockClient);
        $sut->login('test_token');
        $sut->getUnits();
        $requests = $mockClient->getRequests();
        $this->assertCount(2, $requests);
        $getUnitsRequest = $requests[1];
        $expectedBody = 'svc=' . urlencode('core/search_items') .
            '&params=' . urlencode(json_encode([
                'spec' => [
                    'itemsType' => 'avl_unit',
                    'propName' => 'sys_name',
                    'propValueMask' => '*',
                    'sortType' => 'sys_name',
                    'propType' => '',
                ],
                'force' => 1,
                'flags' => 0x101,
                'from' => 0,
                'to' => 0,
            ])) .
            '&sid=' . urlencode($session_id);
        $this->assertSame($expectedBody, (string)$getUnitsRequest->getBody());
    }

    public function test_throws_wialon_exception_when_session_id_is_null(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $this->expectException(WialonException::class);
        $this->expectExceptionMessage('Client does not have session ID.');
        $this->expectExceptionCode(9001);

        $sut->getUnits();
    }

    public function test_returns_expected_value_on_success(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $getUnitsResponseBody = $this->getFixtureContents('get_units_response.json');
        $expected = json_decode($getUnitsResponseBody, true);
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($getUnitsResponseBody) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($getUnitsResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);
        $sut->login('test_token');

        $actual = $sut->getUnits();

        $this->assertSame($expected, $actual);
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_throws_wialon_exception_on_error_response(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected exception'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($responseContents) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $loginResponseBody = $this->getFixtureContents('login_response.json');
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($loginResponseBody));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($responseContents));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            throw new Exception('Unexpected request');
        });

        $this->expectException(WialonException::class);
        $this->expectExceptionMessage($expectedMessage);
        $this->expectExceptionCode($expectedCode);

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);

        $sut->login('test_token');
        $sut->getUnits();
    }

    public function test_throws_expected_wialon_exception_when_transfer_exception_is_thrown(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $previousException = new TransferException('Could not send request.');
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($previousException) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                throw $previousException;
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);
        try {
            $sut->login('test_token');
            $sut->getUnits();
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertSame($previousException, $exception->getPrevious());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail(sprintf('Unexpected exception [%s]: %s', $exceptionClass, $exception->getMessage()));
        }
    }

    public function test_throws_expected_wialon_exception_when_request_exception_is_thrown(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                throw new RequestException('Invalid request', $request);
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);
        try {
            $sut->login('test_token');
            $sut->getUnits();
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(RequestException::class, $exception->getPrevious());
            $this->assertSame('Invalid request', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail(sprintf('Unexpected exception [%s]: %s', $exceptionClass, $exception->getMessage()));
        }
    }

    public function test_throws_expected_wialon_exception_when_network_exception_is_thrown(): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                throw new NetworkException('Network failure', $request);
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);
        try {
            $sut->login('test_token');
            $sut->getUnits();
        } catch (WialonException $exception) {
            $this->assertSame('Service request failed.', $exception->getMessage());
            $this->assertSame(9000, $exception->getCode());
            $this->assertInstanceOf(NetworkException::class, $exception->getPrevious());
            $this->assertSame('Network failure', $exception->getPrevious()->getMessage());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail(sprintf('Unexpected exception [%s]: %s', $exceptionClass, $exception->getMessage()));
        }
    }

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_throws_wialon_exception_when_http_status_code_is_not_success(int $http_status, string $expected_message, int $expected_code): void
    {
        $mockClient = new Client();
        $mockClient->setDefaultException(new Exception('Unexpected request'));
        $requestMatcher = new RequestMatcher('wialon/ajax.html', 'track.agilogistics.net', 'POST', 'https');
        $mockClient->on($requestMatcher, function (RequestInterface $request) use ($http_status) {
            $requestBody = (string)$request->getBody();
            if (str_contains($requestBody, 'svc=' . urlencode('token/login'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('login_response.json')));
                $stubResponse->method('getStatusCode')->willReturn(200);
                return $stubResponse;
            }

            if (str_contains($requestBody, 'svc=' . urlencode('core/search_items'))) {
                $stubResponse = $this->createStub(ResponseInterface::class);
                $stubResponse->method('getStatusCode')->willReturn($http_status);
                return $stubResponse;
            }

            throw new Exception('Unexpected request');
        });

        $sut = new Wialon('https://track.agilogistics.net', $mockClient);
        try {
            $sut->login('test_token');
            $sut->getUnits();
        } catch (WialonException $exception) {
            $this->assertSame($expected_message, $exception->getMessage());
            $this->assertSame($expected_code, $exception->getCode());
        } catch (Throwable $exception) {
            $exceptionClass = get_class($exception);
            $this->fail(sprintf('Unexpected exception [%s]: %s', $exceptionClass, $exception->getMessage()));
        }
    }
}
