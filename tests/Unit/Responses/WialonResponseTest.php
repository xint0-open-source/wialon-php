<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Tests\Unit\Responses;

use GuzzleHttp\Psr7\Utils;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\ProvidesServiceErrors;
use Tests\TestCase;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItem;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItems;
use Xint0\WialonPhp\Responses\GisRoute;
use Xint0\WialonPhp\Responses\WialonResponse;
use Xint0\WialonPhp\WialonException;

#[CoversClass(WialonResponse::class)]
#[UsesClass(GisRoute::class)]
#[UsesClass(WialonException::class)]
#[UsesClass(GisIntelligentSearchItem::class)]
#[UsesClass(GisIntelligentSearchItems::class)]
class WialonResponseTest extends TestCase
{
    use ProvidesServiceErrors;

    #[DataProvider('unexpectedHttpCodeProvider')]
    public function test_constructor_throws_wialon_exception_when_response_interface_status_is_not_success(int $httpStatusCode, string $expectedExceptionMessage, int $expectedExceptionCode): void
    {
        $stubResponseInterface = $this->createStub(ResponseInterface::class);
        $stubResponseInterface->method('getStatusCode')->willReturn($httpStatusCode);
        $this->expectException(WialonException::class);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $this->expectExceptionCode($expectedExceptionCode);

        new WialonResponse(GisRoute::class, $stubResponseInterface);
    }

    public function test_constructor_throws_wialon_exception_when_response_interface_contents_cannot_be_decoded_as_json(): void
    {
        $stubResponseInterface = $this->createStub(ResponseInterface::class);
        $stubResponseInterface->method('getStatusCode')->willReturn(200);
        $stubResponseInterface->method('getBody')->willReturn(Utils::streamFor('{['));
        $this->expectException(WialonException::class);
        $this->expectExceptionCode(9002);
        $this->expectExceptionMessage('Could not decode response.');

        new WialonResponse(GisRoute::class, $stubResponseInterface);
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_constructor_throws_wialon_exception_when_response_has_service_error(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $stubResponseInterface = $this->createStub(ResponseInterface::class);
        $stubResponseInterface->method('getStatusCode')->willReturn(200);
        $stubResponseInterface->method('getBody')->willReturn(Utils::streamFor($responseContents));
        $this->expectException(WialonException::class);
        $this->expectExceptionCode($expectedCode);
        $this->expectExceptionMessage($expectedMessage);

        new WialonResponse(GisRoute::class, $stubResponseInterface);
    }

    public function test_new_instance_returns_expected_gis_route_data(): void
    {
        $stubResponseInterface = $this->createStub(ResponseInterface::class);
        $stubResponseInterface->method('getStatusCode')->willReturn(200);
        $stubResponseInterface->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('gis_route_response.json')));
        $expectedPoints = json_decode($this->getFixtureContents('gis_route_response.json'))->points;

        /** @var WialonResponse<GisRoute> $actual */
        /** @noinspection PhpUnhandledExceptionInspection */
        $actual = new WialonResponse(GisRoute::class, $stubResponseInterface);

        $this->assertSame('OK', $actual->data()->status);
        $this->assertSame($expectedPoints, $actual->data()->points);
        $this->assertSame(18605.4199219, $actual->data()->distance);
        $this->assertSame(1080, $actual->data()->duration);
    }

    public function test_new_instance_returns_expected_gis_intelligent_search_items_data(): void
    {
        $stubResponseInterface = $this->createStub(ResponseInterface::class);
        $stubResponseInterface->method('getStatusCode')->willReturn(200);
        $stubResponseInterface->method('getBody')->willReturn(Utils::streamFor($this->getFixtureContents('gis_intelligent_search_response.json')));
        $expectedItems = new GisIntelligentSearchItems(...array_map(fn ($item) => new GisIntelligentSearchItem($item), json_decode($this->getFixtureContents('gis_intelligent_search_items.json'), true)));

        /** @var WialonResponse<GisIntelligentSearchItems> $actual */
        /** @noinspection PhpUnhandledExceptionInspection */
        $actual = new WialonResponse(GisIntelligentSearchItems::class, $stubResponseInterface);

        $this->assertEquals($expectedItems, $actual->data());
    }
}
