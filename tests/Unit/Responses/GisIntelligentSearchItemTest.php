<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

declare(strict_types=1);

namespace Tests\Unit\Responses;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Tests\TestCase;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItem;

#[CoversClass(GisIntelligentSearchItem::class)]
class GisIntelligentSearchItemTest extends TestCase
{
    public static function invalidValueTypesProvider(): array
    {
        return [
            'invalid latitude' => [
                [
                    'latitude' => false,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "latitude" with value .* is expected to be of type "float", but is of type ".*"\.$/',
            ],
            'invalid longitude' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => '-106.38999939',
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "longitude" with value ".*" is expected to be of type "float", but is of type ".*"\.$/',
            ],
            'invalid house' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => null,
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "house" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid street' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => [],
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "street" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid city' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 1234,
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "city" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid region' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => null,
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "region" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid country' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 1.2,
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "country" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid formatted_path' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => false,
                    'map' => 'map',
                ],
                '/^The option "formatted_path" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid map' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => [],
                ],
                '/^The option "map" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid probability' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                    'probability' => 'test',
                ],
                '/^The option "probability" with value .* is expected to be of type "null" or "float", but is of type ".*"\.$/',
            ],
        ];
    }

    #[DataProvider('invalidValueTypesProvider')]
    public function test_constructor_throws_exception_with_invalid_value_types(array $data, string $expected_message_regex): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessageMatches($expected_message_regex);
        new GisIntelligentSearchItem($data);
    }

    public static function invalidValuesProvider(): array
    {
        return [
            'invalid positive latitude' => [
                [
                    'latitude' => 91.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "latitude" with value .* is invalid.$/',
            ],
            'invalid negative latitude' => [
                [
                    'latitude' => -91.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "latitude" with value .* is invalid.$/',
            ],
            'invalid positive longitude' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => 186.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "longitude" with value .* is invalid.$/',
            ],
            'invalid negative longitude' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -186.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The option "longitude" with value .* is invalid.$/',
            ],
        ];
    }

    #[DataProvider('invalidValuesProvider')]
    public function test_constructor_throws_exception_with_invalid_values(array $data, string $expected_exception_regex): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessageMatches($expected_exception_regex);
        new GisIntelligentSearchItem($data);
    }

    public static function missingValuesProvider(): array
    {
        return [
            'missing latitude' => [
                [
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "latitude" is missing\.$/',
            ],
            'missing longitude' => [
                [
                    'latitude' => 31.708650589,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "longitude" is missing\.$/',
            ],
            'missing house' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "house" is missing\.$/',
            ],
            'missing street' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "street" is missing\.$/',
            ],
            'missing city' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "city" is missing\.$/',
            ],
            'missing region' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "region" is missing\.$/',
            ],
            'missing country' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'formatted_path' => 'street house, city, region, country',
                    'map' => 'map',
                ],
                '/^The required option "country" is missing\.$/',
            ],
            'missing formatted path' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'map' => 'map',
                ],
                '/^The required option "formatted_path" is missing\.$/',
            ],
            'missing map' => [
                [
                    'latitude' => 31.708650589,
                    'longitude' => -106.38999939,
                    'house' => 'house',
                    'street' => 'street',
                    'city' => 'city',
                    'region' => 'region',
                    'country' => 'country',
                    'formatted_path' => 'street house, city, region, country',
                ],
                '/^The required option "map" is missing\.$/',
            ],
        ];
    }

    #[DataProvider('missingValuesProvider')]
    public function test_constructor_throws_exception_with_missing_values(array $data, string $expected_exception_regex): void
    {
        $this->expectException(MissingOptionsException::class);
        $this->expectExceptionMessageMatches($expected_exception_regex);
        new GisIntelligentSearchItem($data);
    }

    public function test_constructor_returns_instance_with_expected_property_values_without_optional_parameters(): void
    {
        $data = [
            'latitude' => 31.708650589,
            'longitude' => -106.38999939,
            'house' => 'house',
            'street' => 'street',
            'city' => 'city',
            'region' => 'region',
            'country' => 'country',
            'formatted_path' => 'street house, city, region, country',
            'map' => 'map',
        ];
        $actual = new GisIntelligentSearchItem($data);
        $this->assertSame($data['latitude'], $actual->latitude);
        $this->assertSame($data['longitude'], $actual->longitude);
        $this->assertSame($data['house'], $actual->house);
        $this->assertSame($data['street'], $actual->street);
        $this->assertSame($data['city'], $actual->city);
        $this->assertSame($data['region'], $actual->region);
        $this->assertSame($data['country'], $actual->country);
        $this->assertSame($data['formatted_path'], $actual->formatted_path);
        $this->assertSame($data['map'], $actual->map);
        $this->assertNull($actual->probability);
    }

    public function test_constructor_returns_instance_with_expected_property_values_with_optional_parameters(): void
    {
        $data = [
            'latitude' => 31.708650589,
            'longitude' => -106.38999939,
            'house' => 'house',
            'street' => 'street',
            'city' => 'city',
            'region' => 'region',
            'country' => 'country',
            'formatted_path' => 'street house, city, region, country',
            'map' => 'map',
            'probability' => 0.543591136732,
        ];
        $actual = new GisIntelligentSearchItem($data);
        $this->assertSame($data['latitude'], $actual->latitude);
        $this->assertSame($data['longitude'], $actual->longitude);
        $this->assertSame($data['house'], $actual->house);
        $this->assertSame($data['street'], $actual->street);
        $this->assertSame($data['city'], $actual->city);
        $this->assertSame($data['region'], $actual->region);
        $this->assertSame($data['country'], $actual->country);
        $this->assertSame($data['formatted_path'], $actual->formatted_path);
        $this->assertSame($data['map'], $actual->map);
        $this->assertSame($data['probability'], $actual->probability);
    }
}
