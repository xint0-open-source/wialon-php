<?php

/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

namespace Tests\Unit\Responses;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Tests\TestCase;
use Xint0\WialonPhp\Responses\GisRoute;

#[CoversClass(GisRoute::class)]
class GisRouteTest extends TestCase
{
    public static function missingValuesProvider(): array
    {
        return [
            'missing status' => [
                [
                    'points' => '',
                    'distance' => [
                        'value' => 18605.4199219,
                    ],
                    'duration' => [
                        'value' => 1080,
                    ],
                ],
                '/^The required option "status" is missing\.$/',
            ],
            'missing points' => [
                [
                    'status' => 'OK',
                    'distance' => [
                        'text' => '18.61 km',
                        'value' => 18605.4199219,
                    ],
                    'duration' => [
                        'text' => '18 min',
                        'value' => 1080,
                    ],
                ],
                '/^The required option "points" is missing\.$/',
            ],
            'missing distance value' => [
                [
                    'status' => 'OK',
                    'points' => '',
                    'distance' => [
                        'text' => '18.61 km',
                    ],
                    'duration' => [
                        'value' => 1080,
                    ],
                ],
                '/^The required option "distance\[value\]" is missing\.$/',
            ],
            'missing duration value' => [
                [
                    'status' => 'OK',
                    'points' => '',
                    'distance' => [
                        'value' => 18605.4199219,
                    ],
                    'duration' => [
                        'text' => '18 min',
                    ],
                ],
                '/^The required option "duration\[value\]" is missing\.$/',
            ],
        ];
    }

    #[DataProvider('missingValuesProvider')]
    public function test_throws_exception_with_missing_values(array $data, string $expected_exception_message_regex): void
    {
        $this->expectException(MissingOptionsException::class);
        $this->expectExceptionMessageMatches($expected_exception_message_regex);
        new GisRoute($data);
    }

    public static function invalidDataTypeProvider(): array
    {
        return [
            'invalid status' => [
                [
                    'status' => 1,
                    'points' => '',
                    'distance' => [
                        'text' => '18.61 km',
                        'value' => 18605.4199219,
                    ],
                    'duration' => [
                        'text' => '18 min',
                        'value' => 1080,
                    ],
                ],
                '/^The option "status" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid points' => [
                [
                    'status' => 'OK',
                    'points' => false,
                    'distance' => [
                        'text' => '18.61 km',
                        'value' => 18605.4199219,
                    ],
                    'duration' => [
                        'text' => '18 min',
                        'value' => 1080,
                    ],
                ],
                '/^The option "points" with value .* is expected to be of type "string", but is of type ".*"\.$/',
            ],
            'invalid distance value' => [
                [
                    'status' => 'OK',
                    'points' => '',
                    'distance' => [
                        'text' => '18.61 km',
                        'value' => '18605.4199219',
                    ],
                    'duration' => [
                        'text' => '18 min',
                        'value' => 1080,
                    ],
                ],
                '/^The option "distance\[value\]" with value .* is expected to be of type "float", but is of type ".*"\.$/',
            ],
            'invalid duration value' => [
                [
                    'status' => 'OK',
                    'points' => '',
                    'distance' => [
                        'text' => '18.61 km',
                        'value' => 18605.4199219,
                    ],
                    'duration' => [
                        'text' => '18 min',
                        'value' => '1080',
                    ],
                ],
                '/^The option "duration\[value\]" with value .* is expected to be of type "int", but is of type ".*"\.$/',
            ],
        ];
    }

    #[DataProvider('invalidDataTypeProvider')]
    public function test_throws_exception_with_invalid_data_type(array $data, string $expected_exception_message_regex): void
    {
        $this->expectException(InvalidOptionsException::class);
        $this->expectExceptionMessageMatches($expected_exception_message_regex);
        new GisRoute($data);
    }

    public function test_new_instance_has_expected_property_values(): void
    {
        $data = [
            'status' => 'OK',
            'points' => 'points_data',
            'distance' => [
                'text' => '18.61 km',
                'value' => 18605.4199219,
            ],
            'duration' => [
                'text' => '18 min',
                'value' => 1080,
            ],
            'extra' => '',
        ];
        $actual = new GisRoute($data);
        $this->assertSame($data['status'], $actual->status);
        $this->assertSame($data['points'], $actual->points);
        $this->assertSame($data['distance']['value'], $actual->distance);
        $this->assertSame($data['duration']['value'], $actual->duration);
    }

    public function test_from_response_json_returns_instance_with_expected_values(): void
    {
        $data = [
            'status' => 'OK',
            'points' => 'points_data',
            'distance' => [
                'text' => '18.61 km',
                'value' => 18605.4199219,
            ],
            'duration' => [
                'text' => '18 min',
                'value' => 1080,
            ],
            'extra' => '',
        ];
        /** @var GisRoute $actual */
        $actual = GisRoute::fromResponseJson($data);
        $this->assertSame($data['status'], $actual->status);
        $this->assertSame($data['points'], $actual->points);
        $this->assertSame($data['distance']['value'], $actual->distance);
        $this->assertSame($data['duration']['value'], $actual->duration);
    }
}
