<?php
/*
 * xint0/wialon-php
 *
 * Wialon API client
 *
 * Copyright (c) 2023. Rogelio Jacinto
 */

namespace Tests\Unit\Responses;

use BadMethodCallException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\TestCase;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItem;
use Xint0\WialonPhp\Responses\GisIntelligentSearchItems;

#[CoversClass(GisIntelligentSearchItems::class)]
#[UsesClass(GisIntelligentSearchItem::class)]
class GisIntelligentSearchItemsTest extends TestCase
{
    public function test_can_create_empty_list(): void
    {
        $sut = new GisIntelligentSearchItems();
        $this->assertCount(0, $sut);
    }

    public function test_can_create_with_one_item(): void
    {
        $expected_item = new GisIntelligentSearchItem([
            'latitude' => 31.708650589,
            'longitude' => -106.38999939,
            'house' => 'house',
            'street' => 'street',
            'city' => 'city',
            'region' => 'region',
            'country' => 'country',
            'formatted_path' => 'street house, city, region, country',
            'map' => 'map',
            'probability' => 0.543591136732,
        ]);
        $sut = new GisIntelligentSearchItems($expected_item);
        $this->assertCount(1, $sut);
        $this->assertTrue(isset($sut[0]));
        $this->assertContains($expected_item, $sut);
        $this->assertSame($expected_item, $sut[0]);
    }

    public function test_can_create_with_multiple_items(): void
    {
        $expected_items = [
            new GisIntelligentSearchItem([
                'latitude' => 31.708650589,
                'longitude' => -106.38999939,
                'house' => 'first',
                'street' => 'street',
                'city' => 'city',
                'region' => 'region',
                'country' => 'country',
                'formatted_path' => 'street house, city, region, country',
                'map' => 'map',
                'probability' => 0.543591136732,
            ]),
            new GisIntelligentSearchItem([
                'latitude' => 31.708650589,
                'longitude' => -106.38999939,
                'house' => 'second',
                'street' => 'street',
                'city' => 'city',
                'region' => 'region',
                'country' => 'country',
                'formatted_path' => 'street house, city, region, country',
                'map' => 'map',
            ]),
            new GisIntelligentSearchItem([
                'latitude' => 31.708650589,
                'longitude' => -106.38999939,
                'house' => 'third',
                'street' => 'street',
                'city' => 'city',
                'region' => 'region',
                'country' => 'country',
                'formatted_path' => 'street house, city, region, country',
                'map' => 'map',
            ]),
        ];
        $sut = new GisIntelligentSearchItems(...$expected_items);
        $this->assertCount(count($expected_items), $sut);
        foreach ($expected_items as $key => $expected_item) {
            $this->assertSame($expected_item, $sut[$key]);
        }
    }

    public function test_throws_bad_method_call_exception_on_set_array_value(): void
    {
        $expected_item = new GisIntelligentSearchItem([
            'latitude' => 31.708650589,
            'longitude' => -106.38999939,
            'house' => 'house',
            'street' => 'street',
            'city' => 'city',
            'region' => 'region',
            'country' => 'country',
            'formatted_path' => 'street house, city, region, country',
            'map' => 'map',
            'probability' => 0.543591136732,
        ]);
        $sut = new GisIntelligentSearchItems($expected_item);
        $this->expectException(BadMethodCallException::class);
        $this->expectExceptionMessage('Cannot set value of read-only list.');

        $sut[0] = $expected_item;
    }

    public function test_throws_bad_method_call_exception_on_unset_array_value(): void
    {
        $expected_item = new GisIntelligentSearchItem([
            'latitude' => 31.708650589,
            'longitude' => -106.38999939,
            'house' => 'house',
            'street' => 'street',
            'city' => 'city',
            'region' => 'region',
            'country' => 'country',
            'formatted_path' => 'street house, city, region, country',
            'map' => 'map',
            'probability' => 0.543591136732,
        ]);
        $sut = new GisIntelligentSearchItems($expected_item);
        $this->expectException(BadMethodCallException::class);
        $this->expectExceptionMessage('Cannot unset value of read-only list.');

        unset($sut[0]);
    }

    public function test_from_response_json_static_method_returns_instance_with_expected_values(): void
    {
        $source_json_array = json_decode($this->getFixtureContents('gis_intelligent_search_response.json'), true);
        $expectedItems = new GisIntelligentSearchItems(...array_map(fn ($item) => new GisIntelligentSearchItem($item), json_decode($this->getFixtureContents('gis_intelligent_search_items.json'), true)));
        $actual = GisIntelligentSearchItems::fromResponseJson($source_json_array);
        $this->assertEquals($expectedItems, $actual);
    }
}
