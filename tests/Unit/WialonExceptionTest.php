<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit;

use Http\Client\Exception\TransferException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\Concerns\ProvidesServiceErrors;
use Xint0\WialonPhp\WialonException;
use Tests\TestCase;

#[CoversClass(WialonException::class)]
class WialonExceptionTest extends TestCase
{
    use ProvidesServiceErrors;

    public function test_no_session_exception_returns_wialon_exception_instance_with_expected_message(): void
    {
        $actual = WialonException::noSessionException();
        $this->assertSame('Client does not have session ID.', $actual->getMessage());
    }

    public function test_no_session_exception_returns_wialon_exception_instance_with_expected_code(): void
    {
        $actual = WialonException::noSessionException();
        $this->assertSame(9001, $actual->getCode());
    }

    public function test_no_session_exception_returns_wialon_exception_instance_with_null_previous_exception(): void
    {
        $actual = WialonException::noSessionException();
        $this->assertNull($actual->getPrevious());
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_service_error_returns_wialon_exception_instance_with_expected_message(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $actual = WialonException::serviceError($expectedCode);
        $this->assertSame($expectedMessage, $actual->getMessage());
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_service_error_returns_wialon_exception_instance_with_expected_code(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $actual = WialonException::serviceError($expectedCode);
        $this->assertSame($expectedCode, $actual->getCode());
    }

    #[DataProvider('serviceErrorProvider')]
    public function test_service_error_returns_wialon_exception_instance_with_null_previous_exception(string $responseContents, int $expectedCode, string $expectedMessage): void
    {
        $actual = WialonException::serviceError($expectedCode);
        $this->assertNull($actual->getPrevious());
    }

    public function test_from_transfer_exception_returns_wialon_exception_instance_with_expected_code(): void
    {
        $previousException = new TransferException('Request failed.');
        $actual = WialonException::fromClientExceptionInterface($previousException);
        $this->assertSame(9000, $actual->getCode());
    }

    public function test_from_transfer_exception_returns_wialon_exception_instance_with_expected_message(): void
    {
        $previousException = new TransferException('Request failed.');
        $actual = WialonException::fromClientExceptionInterface($previousException);
        $this->assertSame('Service request failed.', $actual->getMessage());
    }

    public function test_from_transfer_exception_returns_wialon_exception_instance_with_expected_previous_exception(): void
    {
        $previousException = new TransferException('Request failed.');
        $actual = WialonException::fromClientExceptionInterface($previousException);
        $this->assertSame($previousException, $actual->getPrevious());
    }

    public function test_client_error_http_status_returns_wialon_exception_instance_with_expected_message(): void
    {
        $actual = WialonException::clientErrorHttpCode(400);
        $this->assertSame('Client error code: 400.', $actual->getMessage());
    }

    public function test_client_error_http_code_returns_wialon_exception_instance_with_expected_code(): void
    {
        $actual = WialonException::clientErrorHttpCode(400);
        $this->assertSame(10400, $actual->getCode());
    }

    public function test_client_error_http_code_returns_wialon_exception_instance_with_null_previous_exception(): void
    {
        $actual = WialonException::clientErrorHttpCode(400);
        $this->assertNull($actual->getPrevious());
    }

    public function test_server_error_http_code_returns_wialon_exception_instance_with_expected_message(): void
    {
        $actual = WialonException::serverErrorHttpCode(500);
        $this->assertSame('Server error code: 500.', $actual->getMessage());
    }

    public function test_server_error_http_code_returns_wialon_exception_instance_with_expected_code(): void
    {
        $actual = WialonException::serverErrorHttpCode(500);
        $this->assertSame(20500, $actual->getCode());
    }

    public function test_server_error_http_code_returns_wialon_exception_instance_with_null_previous_exception(): void
    {
        $actual = WialonException::serverErrorHttpCode(500);
        $this->assertNull($actual->getPrevious());
    }

    public function test_unexpected_http_code_returns_wialon_exception_instance_with_expected_message(): void
    {
        $actual = WialonException::unexpectedHttpCode(999);
        $this->assertSame('Unexpected code: 999.', $actual->getMessage());
    }

    public function test_unexpected_http_code_returns_wialon_exception_instance_with_expected_code(): void
    {
        $actual = WialonException::unexpectedHttpCode(999);
        $this->assertSame(30999, $actual->getCode());
    }

    public function test_unexpected_http_code_returns_wialon_exception_instance_with_null_previous_exception(): void
    {
        $actual = WialonException::unexpectedHttpCode(999);
        $this->assertNull($actual->getPrevious());
    }
}
