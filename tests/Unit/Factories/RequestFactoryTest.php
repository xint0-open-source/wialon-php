<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

namespace Tests\Unit\Factories;

use Http\Discovery\Psr17FactoryDiscovery;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Xint0\WialonPhp\Factories\RequestFactory;
use Tests\TestCase;
use Xint0\WialonPhp\Requests\AddressFormat;
use Xint0\WialonPhp\Requests\GisGeocodeRequestParameters;
use Xint0\WialonPhp\Requests\GisGetRouteParameters;
use Xint0\WialonPhp\Requests\GisIntelligentSearchParameters;

#[CoversClass(RequestFactory::class)]
#[UsesClass(AddressFormat::class)]
#[UsesClass(GisGeocodeRequestParameters::class)]
#[UsesClass(GisIntelligentSearchParameters::class)]
#[UsesClass(GisGetRouteParameters::class)]
class RequestFactoryTest extends TestCase
{
    public function test_new_instance_has_expected_default_base_uri(): void
    {
        $expected = 'https://hst-api.wialon.com';
        $sut = new RequestFactory();
        $this->assertSame($expected, (string)$sut->baseUri());
    }

    public function test_new_instance_has_expected_custom_base_uri(): void
    {
        $expected = 'https://track.agilogistics.net';
        $sut = new RequestFactory($expected);
        $this->assertSame($expected, (string)$sut->baseUri());
    }

    public function test_make_login_request_returns_request_interface_with_post_http_method(): void
    {
        $token = 'test_token';
        $sut = new RequestFactory();
        $actual = $sut->makeLoginRequest($token);
        $this->assertSame('POST', $actual->getMethod());
    }

    public function test_make_login_request_returns_request_interface_with_expected_path_in_uri(): void
    {
        $token = 'test_token';
        $sut = new RequestFactory();
        $actual = $sut->makeLoginRequest($token);
        $this->assertSame('wialon/ajax.html', $actual->getUri()->getPath());
    }

    public function test_make_login_request_returns_request_interface_with_expected_headers(): void
    {
        $token = 'test_token';
        $expected = [
            'Host' => ['hst-api.wialon.com'],
            'Accept' => ['application/json'],
            'Content-Type' => ['application/x-www-form-urlencoded'],
        ];
        $sut = new RequestFactory();
        $actual = $sut->makeLoginRequest($token);

        $this->assertSame($expected, $actual->getHeaders());
    }

    public function test_make_login_request_returns_request_interface_with_expected_body(): void
    {
        $token = 'test_token';
        $expected = 'svc=' . urlencode('token/login') . '&params=' . urlencode(json_encode(compact('token')));
        $sut = new RequestFactory();
        $actual = $sut->makeLoginRequest($token);

        $this->assertSame($expected, (string)$actual->getBody());
    }

    public function test_make_logout_request_returns_request_interface_with_expected_headers(): void
    {
        $session_id = 'test_session_id';
        $expected = [
            'Host' => ['hst-api.wialon.com'],
            'Accept' => ['application/json'],
            'Content-Type' => ['application/x-www-form-urlencoded'],
        ];
        $sut = new RequestFactory();
        $actual = $sut->makeLogoutRequest($session_id);

        $this->assertSame($expected, $actual->getHeaders());
    }

    public function test_make_logout_request_returns_request_interface_with_expected_body(): void
    {
        $session_id = 'test_session_id';
        $expected = 'svc=' . urlencode('core/logout') . '&params=' . urlencode('{}') . '&sid=' . urlencode($session_id);
        $sut = new RequestFactory();
        $actual = $sut->makeLogoutRequest($session_id);

        $this->assertSame($expected, (string)$actual->getBody());
    }

    public function test_make_search_items_request_returns_request_interface_with_expected_headers(): void
    {
        $session_id = 'test_session_id';
        $params = [
            'spec' => [
                'itemsType' => 'avl_unit',
                'propName' => 'sys_name',
                'propValueMask' => 'test',
                'sortType' => '',
                'propType' => '',
                'or_logic' => false,
            ],
            'force' => 1,
            'flags' => 0x411,
            'from' => 0,
            'to' => 0,
        ];

        $expected = [
            'Host' => ['hst-api.wialon.com'],
            'Accept' => ['application/json'],
            'Content-Type' => ['application/x-www-form-urlencoded'],
        ];
        $sut = new RequestFactory();
        $actual = $sut->makeSearchItemsRequest($params, $session_id);

        $this->assertSame($expected, $actual->getHeaders());
    }

    public function test_make_search_items_request_returns_request_interface_with_expected_body(): void
    {
        $session_id = 'test_session_id';
        $params = [
            'spec' => [
                'itemsType' => 'avl_unit',
                'propName' => 'sys_name',
                'propValueMask' => 'test',
                'sortType' => '',
                'propType' => '',
                'or_logic' => false,
            ],
            'force' => 1,
            'flags' => 0x411,
            'from' => 0,
            'to' => 0,
        ];
        $expected = 'svc=' . urlencode('core/search_items') .
            '&params=' . urlencode(json_encode($params)) .
            '&sid=' . urlencode($session_id);
        $sut = new RequestFactory();

        $actual = $sut->makeSearchItemsRequest($params, $session_id);
        $this->assertSame($expected, (string)$actual->getBody());
    }

    public function test_make_gis_geocode_request_returns_request_interface_with_expected_uri_path(): void
    {
        $params = new GisGeocodeRequestParameters(-111.004287, 30.6141861, new AddressFormat('321'), 1234);
        $expected = 'gis_geocode';
        $sut = new RequestFactory();

        $actual = $sut->makeGisGeocodeRequest($params);
        $this->assertSame($expected, $actual->getUri()->getPath());
    }

    public function test_make_gis_geocode_request_returns_request_interface_with_expected_body(): void
    {
        $params = new GisGeocodeRequestParameters(-111.004287, 30.6141861, new AddressFormat('321'), 1234);
        $expected = 'coords=' . urlencode('[{"lon":-111.004287,"lat":30.6141861}]') . '&flags=876609536&uid=1234';
        $sut = new RequestFactory();

        $actual = $sut->makeGisGeocodeRequest($params);
        $this->assertSame($expected, (string)$actual->getBody());
    }

    public function test_make_geofences_by_resource_id_request_returns_request_interface_with_expected_headers(): void
    {
        $expected_headers = [
            'Host' => ['hst-api.wialon.com'],
            'Accept' => ['application/json'],
            'Content-Type' => ['application/x-www-form-urlencoded'],
        ];

        $session_id = 'test_session_id';
        $resource_id = 1234;
        $sut = new RequestFactory();
        $actual = $sut->makeGeofencesByResourceIdRequest($resource_id, $session_id);
        $this->assertSame($expected_headers, $actual->getHeaders());
    }

    public function test_make_geofences_by_resource_id_request_returns_request_interface_with_expected_body(): void
    {
        $session_id = 'test_session_id';
        $resource_id = 1234;
        $params = [
            'itemId' => $resource_id,
            'flags' => 0x10,
        ];
        $expected_body = 'svc=' . urlencode('resource/get_zone_data') .
            '&params=' . urlencode(json_encode($params)) .
            '&sid=' . urlencode($session_id);

        $sut = new RequestFactory();
        $actual = $sut->makeGeofencesByResourceIdRequest($resource_id, $session_id);
        $this->assertSame($expected_body, (string)$actual->getBody());
    }

    public function test_make_geofences_by_resource_id_request_returns_request_interface_with_expected_body_when_geofence_ids_exists(): void
    {
        $session_id = 'test_session_id';
        $resource_id = 1234;
        $params = [
            'itemId' => $resource_id,
            'flags' => 0x18,
            'col' => [98, 99],
        ];
        $expected_body = 'svc=' . urlencode('resource/get_zone_data') .
            '&params=' . urlencode(json_encode($params)) .
            '&sid=' . urlencode($session_id);

        $sut = new RequestFactory();
        $actual = $sut->makeGeofencesByResourceIdRequest($resource_id, $session_id, [98, 99], 0x18);
        $this->assertSame($expected_body, (string)$actual->getBody());
    }

    public function test_base_uri_sets_base_uri_value_from_string(): void
    {
        $sut = new RequestFactory();
        $expected_base_uri_string = 'http://example.com';
        $actual = $sut->baseUri($expected_base_uri_string);
        $this->assertSame($expected_base_uri_string, (string)$actual);
        $this->assertSame('http', $actual->getScheme());
        $this->assertSame('example.com', $actual->getHost());
    }

    public function test_base_uri_sets_base_uri_value_from_uri_interface(): void
    {
        $sut = new RequestFactory();
        $expected_base_uri = Psr17FactoryDiscovery::findUriFactory()->createUri()
            ->withScheme('https')
            ->withHost('example.com');
        $actual = $sut->baseUri($expected_base_uri);
        $this->assertSame($expected_base_uri, $actual);
    }

    public function test_base_uri_does_not_change_base_uri_value_when_new_value_is_null(): void
    {
        $expected_base_uri = Psr17FactoryDiscovery::findUriFactory()->createUri('https://example.com');
        $sut = new RequestFactory();
        $sut->baseUri($expected_base_uri);
        /** @noinspection PhpRedundantOptionalArgumentInspection */
        $actual = $sut->baseUri(null);
        $this->assertSame($expected_base_uri, $actual);
    }

    public function test_base_uri_does_not_change_base_uri_value_when_new_value_does_not_have_scheme(): void
    {
        $sut = new RequestFactory();
        $original = $sut->baseUri();
        $actual = $sut->baseUri('//example.com');
        $this->assertSame($original, $actual);
    }

    public function test_base_uri_does_not_change_base_uri_value_when_new_value_does_not_have_host(): void
    {
        $sut = new RequestFactory();
        $original = $sut->baseUri();
        $new_base_uri = Psr17FactoryDiscovery::findUriFactory()->createUri()->withScheme('test')->withHost('');
        $actual = $sut->baseUri($new_base_uri);
        $this->assertNotSame($new_base_uri, $actual);
        $this->assertSame($original, $actual);
    }

    public function test_constructor_throws_invalid_argument_exception_when_base_uri_is_not_valid(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The base URI value "//example.com" is not valid.');
        new RequestFactory('//example.com');
    }

    public function test_constructor_throws_invalid_argument_exception_when_base_uri_cannot_be_parsed(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        try {
            new RequestFactory('scheme://host.com:test/');
        } catch (\Throwable $exception) {
            $this->assertNotNull($exception->getPrevious());
            throw $exception;
        }
    }

    public function test_new_instance_with_uri_interface_custom_base_uri(): void
    {
        $expected = Psr17FactoryDiscovery::findUriFactory()->createUri('https://example.com:8080');
        $sut = new RequestFactory($expected);
        $this->assertSame($expected, $sut->baseUri());
    }

    public function test_base_uri_does_not_change_base_uri_value_when_new_value_cannot_be_parsed(): void
    {
        $sut = new RequestFactory();
        $original = $sut->baseUri();
        $actual = $sut->baseUri('scheme://host.com:test/');
        $this->assertSame($original, $actual);
    }

    public function test_make_gis_intelligent_search_request_returns_request_interface_with_expected_uri_path(): void
    {
        $params = new GisIntelligentSearchParameters('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32476, México', 1234);
        $expected_path = 'gis_searchintelli';

        $sut = new RequestFactory();
        $request = $sut->makeGisIntelligentSearchRequest($params);

        $this->assertSame($expected_path, $request->getUri()->getPath());
    }

    public function test_make_gis_intelligent_search_request_returns_request_interface_with_expected_body(): void
    {
        $params = new GisIntelligentSearchParameters('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32476, México', 1234);
        $expected_body = 'phrase=' . urlencode('Campos Elíseos 9050, Campos Elíseos, Juárez, Chihuahua 32476, México') . '&count=10&uid=1234';

        $sut = new RequestFactory();
        $request = $sut->makeGisIntelligentSearchRequest($params);

        $this->assertSame($expected_body, $request->getBody()->getContents());
    }

    public function test_make_gis_get_route_request_returns_request_interface_with_expected_attributes(): void
    {
        $params = new GisGetRouteParameters(31.7132597, -106.3956615, 31.8120716, -106.3971567, 1, 5794);
        $expected_body = 'lat1=31.7132597&lon1=-106.3956615&lat2=31.8120716&lon2=-106.3971567&flags=1&uid=5794';

        $sut = new RequestFactory();
        $actual = $sut->makeGisGetRouteRequest($params);

        $this->assertSame('POST', $actual->getMethod());
        $this->assertSame('gis_get_route', $actual->getUri()->getPath());
        $this->assertSame($expected_body, $actual->getBody()->getContents());
    }
}
