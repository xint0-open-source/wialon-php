<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit\Factories;

use Http\Discovery\ClassDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use Http\Mock\Client;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Xint0\WialonPhp\Factories\HttpClientFactory;

#[CoversClass(HttpClientFactory::class)]
class HttpClientFactoryTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        ClassDiscovery::prependStrategy(MockClientStrategy::class);
    }

    public function test_make_method_returns_http_client_interface(): void
    {
        $sut = new HttpClientFactory();
        $actual = $sut->make();
        Assert::assertInstanceOf(Client::class, $actual);
    }
}
