<?php

/**
 * xint0/wialon-php
 *
 * Wialon API client.
 *
 * @author Rogelio Jacinto
 * @copyright 2022 Rogelio Jacinto
 * @license https://gitlab.com/xint0-open-source/wialon-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Unit;

use Http\Discovery\ClassDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use Http\Mock\Client;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\TestCase;
use Xint0\WialonPhp\Factories\HttpClientFactory;
use Xint0\WialonPhp\Factories\RequestFactory;
use Xint0\WialonPhp\Wialon;

#[CoversClass(Wialon::class)]
#[UsesClass(HttpClientFactory::class)]
#[UsesClass(RequestFactory::class)]
class WialonTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        ClassDiscovery::prependStrategy(MockClientStrategy::class);
    }

    public function test_creates_default_http_client_when_not_specified(): void
    {
        $sut = new Wialon();
        $actual = $sut->httpClient();
        Assert::assertInstanceOf(Client::class, $actual);
    }

    public function test_uses_http_client_when_specified(): void
    {
        $httpClient = new Client();
        $sut = new Wialon(null, $httpClient);
        $this->assertSame($httpClient, $sut->httpClient());
    }

    public function test_base_uri_returns_default_value_when_not_specified(): void
    {
        $sut = new Wialon();
        $this->assertSame('https', $sut->baseUri()->getScheme());
        $this->assertSame('hst-api.wialon.com', $sut->baseUri()->getHost());
    }

    public function test_base_uri_changes_the_base_uri_value(): void
    {
        $sut = new Wialon();
        $sut->baseUri('http://example.com/test');
        $this->assertSame('http', $sut->baseUri()->getScheme());
        $this->assertSame('example.com', $sut->baseUri()->getHost());
        $this->assertSame('/test', $sut->baseUri()->getPath());
    }
}
